<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<c:if test="${sessionScope['role'] == 'admin'}">
    <c:redirect url="/getAllAccounts" />
</c:if>
<c:if test="${sessionScope['role'] == 'customer'}">
    <c:redirect url="/getCustomerById" />
</c:if>
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<meta name="description" content="">
<meta name="author" content="">
<title>Login</title>
<!-- Bootstrap core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- Custom styles for this template -->
<link href="resources/css/sb-admin.css" rel="stylesheet">
<link href="resources/css/style.css" rel="stylesheet">

</head>

<body>
	<div class="container">
		<div class="card card-login mx-auto mt-5">
		<div class="text-center"><img src = "resources/img/SSTlogo.png" height="150" width="150"></div>
			<div class="card-body">
				<form action="LoginController" method="post">
					<div class="form-group">
						<label for="exampleInputEmail1">Account number</label> <input
							type="text" class="form-control" name="accountNo"
							aria-describedby="emailHelp" placeholder="Account Number"
							pattern="[0-9]+" maxlength="8" minlength="6"
							title="Please enter valid account number" required>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label> <input
							type="password" class="form-control" name="password"
							placeholder="Password" required>
					</div>
					<c:if test="${null != error}">
						<div class="form-group" style="color: red;">
							<c:out value="${error}" />
							<br>
						</div>
					</c:if>
					<input type="submit" class="btn btn-primary btn-block"
						value="Login" name="login">
				</form>
				<div class="text-center">
					<a class="d-block small mt-3" href="createCustomer">Register a
						Customer</a> <a class="d-block small" href="forgot-password">Forgot
						Password?</a>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${success != null}">
		<script>
			var success = "${success}";
			alert(success);
		</script>
	</c:if>
	<!-- Bootstrap core JavaScript -->
	<script src="resources/vendor/jquery/jquery.min.js"></script>
	<script src="resources/vendor/popper/popper.min.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>

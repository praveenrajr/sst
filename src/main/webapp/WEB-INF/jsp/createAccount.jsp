<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="header.jsp"%>
<%@ include file="footer.jsp"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<c:if test="${sessionScope['role'] != 'admin'}">
    <c:redirect url="login" />
</c:if>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<meta name="description" content="">
<meta name="author" content="">
<title>Welcome Admin</title>

<!-- Bootstrap core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Bootstrap core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="resources/css/datepicker.css" rel="stylesheet">
<link href="resources/css/style.css" rel="stylesheet">

<link href="resources/img/glyphicons-halflings.png">
<link href="resources/css/datepicker3.css" rel="stylesheet">
<link href="resources/fonts/glyphicons-halflings-regular.svg"
	rel="stylesheet">

<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- Custom styles for this template -->
<link href="resources/css/sb-admin.css" rel="stylesheet">
<!-- Custom fonts for this template -->
<link href="resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- Plugin CSS -->
<link href="resources/vendor/datatables/dataTables.bootstrap4.css"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="resources/css/sb-admin.css" rel="stylesheet">
<link href="resources/css/style.css" rel="stylesheet">

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
	<!-- Navigation -->
	<c:set var="root" value="${pageContext.request.contextPath}" />
	<div class="container">
		<div class="card card-register mx-auto mt-1">
			<div class="card-header">Register an Account</div>
			<div class="card-body">
				<form:form id="createAccount" action="${root}/addAccount"
					modelAttribute="account" method="post">
					<div class="form-group">
						<div class="form-row">
							<label>Date</label>
							<div class="col-md-8">
								<div class="input-group input-append date" id="datePicker">
									<form:input path="dateOpened" type="text" class="form-control" />
									<i class="input-group-addon"> <i
										class="fa fa-calendar"> </i>
									</i>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-8">
								<label for="amount">Account Type </label>
								<div class="drop-down">
									<form:select path="accountType.id"
										class="btn btn-default dropdown-toggle" required="required">
										<option value=""> --Select --</option>
										<c:forEach items="${types}" var="type">
											<form:option value="${type.id}">${type.type}</form:option>
										</c:forEach>
									</form:select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-8">
								<label for="amount">Amount </label>
								<form:input path="amount" class="form-control"
									pattern="[0-9]{3,6}" maxlength="10" id="number"
									data-validation-error-msg="Amount not valid"
									title="Amount should be in numbers(Minimum 3 digits)"
									required="required" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-8">
								<label for="IFCCode">IFC code</label>
								<form:input path="IFCCode" type="text" class="form-control"
									id="IFFCode" placeholder="IFCCode" required="required" />
							</div>
						</div>
					</div>
					<button class="btn btn-primary" type="submit">Create</button>
				</form:form>
			</div>
		</div>

	</div>

	<!-- Bootstrap core JavaScript -->
	<script src="resources/vendor/jquery/jquery.min.js"></script>
	<script src="resources/vendor/popper/popper.min.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="resources/vendor/jquery/jquery.min.js"></script>
	<script src="resources/js/datepicker.js"></script>

	<script src="resources/js/bootstrap-datetimepicker.min.js"></script>

	<!-- Custom scripts for this template -->
	<script src="js/sb-admin.min.js"></script>
	<!-- Plugin JavaScript -->
	<script src="resources/vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="resources/vendor/chart.js/Chart.min.js"></script>
	<script src="resources/vendor/datatables/jquery.dataTables.js"></script>
	<script src="resources/vendor/datatables/dataTables.bootstrap4.js"></script>
	<script src="resources/js/accountDate.js">
		
	</script>
	<c:if test="${error != null}">
		<script>
			var Msg = '${error}';
			alert(Msg);
		</script>
	</c:if>
	<!-- Custom scripts for this template -->
	<script src="resources/js/sb-admin.min.js"></script>
</body>

</html>

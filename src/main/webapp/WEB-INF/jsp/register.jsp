<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<meta name="description" content="">
<meta name="author" content="">
<title>Register Customer</title>

<!-- Bootstrap core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="resources/css/datepicker.css" rel="stylesheet">
<link href="resources/css/style.css" rel="stylesheet">

<link href="resources/img/glyphicons-halflings.png">
<link href="resources/css/datepicker3.css" rel="stylesheet">
<link href="resources/fonts/glyphicons-halflings-regular.svg"
	rel="stylesheet">

<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- Custom styles for this template -->
<link href="resources/css/sb-admin.css" rel="stylesheet">

</head>

<body style="background-color: #DCDCDC">
	<div class="container">
		<div class="card card-register mx-auto">
			<div class="card-header">Register an Customer</div>
			<div class="card-body">
				<form:form id="registration" action="register" method="post"
					modelAttribute="customer" onsubmit="return ValidateContactForm();">

					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<label for="FirstName" class="required">First Name</label>
								<form:input path="firstName" class="form-control" id="FirstName"
									placeholder="First Name" pattern="[a-zA-z]*" maxlength="20"
									title="First name in alphabets(spaces not allowed)"
									required="required" />
							</div>
							<div class="col-md-6">
								<label for="LastName" class="required">Last Name</label>
								<form:input path="lastName" class="form-control" id="lastName"
									placeholder="Last Name" pattern="[a-zA-z]*" maxlength="20"
									title="Last name in alphabets(spaces not allowed)"
									required="required" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="required">Email Address</label>
						<form:input path="email" type="email" class="form-control"
							id="email" placeholder="Email" required="required" />
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<label for="password" class="required">Password</label>
								<form:input path="password" type="password" class="form-control"
									id="password" onkeyup="checkPass(); return false;" placeholder="Password" name="password"
									title="Minimum 6 characters" minlength="6" maxlength="20"
									required="required" />
							</div>
							<div class="col-md-6">
								<label for="confirmPassword" class="required">Confirm
									Password </label> <input class="form-control" id="confirmPassword"
									onkeyup="checkPass(); return false;" name="confirmPassword" placeholder="Confirm Password"
									required="required" /> <span
                                   id="confirmMessage" class="confirmMessage"></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="Account No" class="required">Account Number</label>
						<form:input path="account.accountNo" class="form-control"
							id="accountNo" placeholder="Account Number" pattern="[0-9]{6,8}"
							required="required" title="Account number should be in 8 digits"
							maxlength="8" />
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<label for="dob">DOB</label>
								<div class="input-group input-append date" id="datePicker">
									<form:input path="DOB" type="text" class="form-control"
										placeholder="yyyy-mm-dd" name=" date" />
									<i class="input-group-addon"> <i
                                        class="fa fa-calendar"> </i>
                                    </i>
								</div>
							</div>
							<div class="col-md-6">
								&emsp; <label for="Gender">Gender</label> <br>&emsp;
								<form:radiobutton path="gender" value="M" />
								Male <label class="radio-inline">&emsp; &emsp;<form:radiobutton
										path="gender" value="F" /> Female
								</label> <br />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-6">
								<label for="Phone">Contact Number</label>
								<form:input path="phoneNo" class="form-control" id="phoneNo"
									placeholder="Contact Number" pattern="[0-9]{10,13}"
									title="Phone should be in 10 digits" maxlength="10"
									required="required" />
							</div>
							<div class="col-md-6">
								<label for="Adhaar No" class="required">Adhaar Number</label>
								<form:input path="adhaarNo" class="form-control" id="adhaarNo"
									placeholder="Adhaar Number" pattern="[0-9]{12}"
									required="required"
									title="Adhaar number should be in 12 digits" maxlength="12" />
							</div>
						</div>
					</div>
					<button class="btn btn-primary btn-block">Register</button>
					<div class="text-center">
					<a class="d-block small mt-3" href="login">Login Page
                            </a>
                            </div>
				</form:form>
			</div>
		</div>
	</div>
	<c:if test="${error != null}">
		<script>
			var Msg = '${error}';
			alert(Msg);
		</script>
	</c:if>
	
	<!-- Bootstrap core JavaScript -->
	<script src="resources/vendor/jquery/jquery.min.js"></script>
	<script src="resources/js/datepicker.js"></script>
	<script src="resources/js/index.js"></script>
	<script src="resources/js/bootstrap-datetimepicker.min.js"></script>
	<script src="resources/js/validate.js"></script>
	<script src="resources/vendor/popper/popper.min.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="resources/js/customerDate.js"></script>
	
	
	<script src="resources/vendor/jquery/jquery.min.js"></script>
    <script src="resources/vendor/popper/popper.min.js"></script>
    <script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="resources/vendor/jquery/jquery.min.js"></script>
    <script src="resources/js/datepicker.js"></script>

    <script src="resources/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/sb-admin.min.js"></script>
    <!-- Plugin JavaScript -->
    <script src="resources/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="resources/vendor/chart.js/Chart.min.js"></script>
    <script src="resources/vendor/datatables/jquery.dataTables.js"></script>
    <script src="resources/vendor/datatables/dataTables.bootstrap4.js"></script>
    <script src="resources/js/sb-admin.min.js"></script>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<c:if test="${sessionScope['role'] != 'customer'}">
    <c:redirect url="login" />
</c:if>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<meta name="description" content="">
<meta name="author" content="">
<title>Welcome Customer</title>

<!-- Bootstrap core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- Plugin CSS -->
<link href="resources/vendor/datatables/dataTables.bootstrap4.css"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="resources/css/sb-admin.css" rel="stylesheet">
<link href="resources/css/style.css" rel="stylesheet">

<script type="text/javascript">
	function alertName() {
		var success = '${success}';
		var failure = '${error}';
		var successAddress = '${successAddress}';
		if (success) {
			alert(success);
		}
		if (failure) {
			alert(failure);
		} 
		if (successAddress) {
			alert(successAddress);
			window.location="/SST/getCustomerById?action=customerview"
		}
	}
</script>
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top"
		id="mainNav">
		<a class="navbar-brand" href="#">Service Secure Transaction</a>
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarResponsive"
			aria-controls="navbarResponsive" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Link"><a class="nav-link"> <i
						class="fa fa-fw fa-fw fa-user" aria-hidden="true"></i> <span
						style="font-weight: bold; color: white;"> ${name}</span>
				</a></li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Beneficiary"><a
					class="nav-link nav-link-collapse collapsed" data-toggle="collapse"
					href="#collapseComponents" data-parent="#exampleAccordion"> <i
						class="fa fa-fw fa-link"></i> <span class="nav-link-text">
							Beneficiary</span>
				</a>
					<ul class="sidenav-second-level collapse" id="collapseComponents">
						<li><a href="viewbeneficiaries?action=beneficiary"><i
								class="fa fa-fw fa-eye" aria-hidden="true"></i> View all</a></li>
						<li><a href="requestbeneficiary"><i
								class="fa fa-fw fa-code-fork" aria-hidden="true"></i> Request</a></li>
					</ul>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Transaction"><a class="nav-link"
					href="viewbeneficiaries?action=transaction"> <i
						class="fa fa-fw fa-exchange" aria-hidden="true"></i> <span
						class="nav-link-text"> Transaction</span>
				</a></li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Profile"><a class="nav-link"
					href="getCustomerById?action=customerview"> <i
						class="fa fa-fw fa-user"></i> <span class="nav-link-text">
							Profile</span>
				</a></li>
			</ul>
			<ul class="navbar-nav sidenav-toggler">
				<li class="nav-item"><a class="nav-link text-center"
					id="sidenavToggler"> <i class="fa fa-fw fa-angle-left"></i>
				</a></li>
			</ul>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link" data-toggle="modal"
					data-target="#exampleModal" data-toggle="tooltip" title="Logout">
						<i class="fa fa-fw fa-sign-out"></i> Logout
				</a></li>
			</ul>
		</div>
	</nav>


	<div class="content-wrapper">
		<c:set var="customer" value="${customer}" />
		<!-- Breadcrumbs -->
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><c:out
					value="${customer.account.getAccountType().getType()} :" /></li>
			<li class="breadcrumb-item active">Total Available Balance INR:
				<c:out value="${customer.account.getAmount()}" />
			</li>
		</ol>
		<c:if test="${customer != null}">
			<div class="mb-0 mt-4">
				<table class="table table-bordered" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Account No</th>
							<th>IFC-code</th>
							<th>Name</th>
							<th>Available Balance</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><c:out value="${customer.account.getAccountNo()}" /></td>
							<td><c:out value="${customer.account.getIFCCode()}" /></td>
							<td><c:out
									value="${customer.getFirstName()} ${customer.getLastName()}" /></td>
							<td><c:out value="${customer.account.getAmount()}" /></td>
							<td><a href="viewtransactions" data-toggle="tooltip"
								title="ViewTransaction"><i class="fa fa-eye"
									aria-hidden="true"></i> </a></td>
						</tr>
					</tbody>
				</table>
		</c:if>
		<c:if test="${address != null}">
			<div class="container">
				<div class="card card-register mx-auto mt-1">
					<div class="card-header">Register an Address</div>
					<div class="card-body">
						<form:form id="address" action="addAddress"
							modelAttribute="address" method="post">
							<div class="form-group">
								<div class="form-row">
									<div class="col-md-8">
										<form:input path="customer.id" type="hidden"
											class="form-control" id="customerId" value="${id}"
											required="required" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="form-row">
									<div class="col-md-8">
										<label for="amount">Street</label>
										<div class="drop-down">
											<form:input path="street" type="text" class="form-control"
												id="street" placeholder="Street" required="required" maxlength="50"/>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="form-row">
									<div class="col-md-8">
										<label for="state">State</label>
										<form:input path="state" class="form-control" id="state"
											placeholder="State" required="required" pattern="[A-Za-z]*" maxlength="30"/>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="form-row">
									<div class="col-md-8">
										<label for="State">City</label>
										<form:input path="city" type="text" class="form-control"
											id="city" placeholder="City" required="required" pattern="[A-Za-z]*" maxlength="30"/>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="form-row">
									<div class="col-md-8">
										<label for="IFCCode">Zipcode</label>
										<form:input path="zipcode" type="text" class="form-control"
											id="zipcode" placeholder="Zipcode" required="required" maxlength="13"/>
									</div>
								</div>
							</div>
							<button class="btn btn-primary" type="submit">Register</button>
						</form:form>
					</div>
				</div>
			</div>
		</c:if>
		<c:if test="${account != null}">
			<div class="card mb-3">
				<div class="card-header">
					<i class="fa fa-table"></i> Last Transactions
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered" width="100%" id="dataTable"
							cellspacing="0">
							<thead>
								<tr>
									<th>Date</th>
									<th>Narration</th>
									<th>Withdrawal</th>
									<th>Deposit</th>
									<th>Closing Balance</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${account.getTransactions()}"
									var="transaction">
									<tr>
										<td><c:out value="${transaction.getDate()}" /></td>
										<td><c:out value="${transaction.getDescription()}" /></td>
										<td><c:out value="${transaction.getWithdraw()}" /></td>
										<td><c:out value="${transaction.getDeposit()}" /></td>
										<td><c:out value="${transaction.getClosing_Balance()}" /></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
	<hr class="mt-2">
	<c:if test="${beneficiaries != null}">
		<div class="container">
			<div class="card card-register mx-auto mt-5">
				<div class="card-header">Transaction</div>
				<div class="card-body">
					<form:form id="transferAmount" action="transaction" method="post"
						modelAttribute="transaction">
						<div class="form-group">
							<label for="beneficiary">Beneficiary</label>
							<div class="drop-down">
								<form:select path="beneficiary.id" required="required"
									class="btn btn-default dropdown-toggle">
									<option value=""> --Select --</option>
									<c:forEach items="${beneficiaries}" var="beneficiary">
										<form:option value="${beneficiary.id}">${beneficiary.name}-${beneficiary.accountNo}</form:option>
									</c:forEach>
								</form:select>
							</div>
						</div>
						<div class="form-group">
							<div class="form-row">
								<label for="description">Description</label>
								<form:input path="description" class="form-control"
									id="description" pattern="[A-Z a-z]*" maxlength="30"
									required="required" title="Description must be in alphabet"/>
							</div>
						</div>
						<div class="form-group">
							<label for="amount">Amount</label>
							<form:input path="withdraw" class="form-control" id="amount"
								placeholder="0.00" pattern="[0-9]+" maxlength="10"
								required="required" title="Please enter the valid amount"/>
						</div>
						<input type="submit" class="btn btn-primary btn-block"
							value="Continue">
					</form:form>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${personalDetails != null}">
		<div class="container">
			<div class="card card-register mx-auto mt-1">
				<div class="card-header">Personal Information</div>
				<div class="card-body">
					<table class="table">
						<tr>
							<td>Id</td>
							<td><c:out value="${personalDetails.getId()}" /></td>
						</tr>
						<tr>
							<td>FirstName</td>
							<td><c:out value="${personalDetails.getFirstName()}" /></td>
						</tr>

						<tr>
							<td>LastName</td>
							<td><c:out value="${personalDetails.getLastName()}" /></td>
						</tr>

						<tr>
							<td>DOB</td>
							<td><c:out value="${personalDetails.getDOB()}" /></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><c:out value="${personalDetails.getEmail()}" /></td>
						</tr>
						<tr>
							<td>AdhaarNo</td>
							<td><c:out value="${personalDetails.getAdhaarNo()}" /></td>
						</tr>
						<tr>
							<td>AccountNo</td>
							<td><c:out
									value="${personalDetails.getAccount().getAccountNo()}" /></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="card card-register mx-auto mt-1">
				<div class="card-header">
					Address<a href="createAddress" data-toggle="tooltip"
						title="Add Address"> <i class="fa fa-address-book"
						aria-hidden="true"></i></a>
				</div>
				<div class="card-body">
					<c:set var="addresses" value="${personalDetails.getAddresses()}" />
					<table class="table">
						<th>Street</th>
						<th>City</th>
						<th>State</th>
						<th>Zipcode</th>
						<c:forEach items="${addresses}" var="address">
							<tr>
								<td><c:out value="${address.getStreet()}" /></td>
								<td><c:out value="${address.getCity()}" /></td>
								<td><c:out value="${address.getState()}" /></td>
								<td><c:out value="${address.getZipcode()}" /></td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
		</div>
	</c:if>

	<!-- /Card Columns -->

	<!-- Example Notifications Card -->

	<!-- Example Tables Card -->
	<c:if test="${null != customerbeneficiaries}">
		<div class="card mb-3">
			<div class="card-header">
				<i class="fa fa-table"></i> List Of Beneficiaries :
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered" width="100%" id="dataTable"
						cellspacing="0">
						<thead>
							<tr>
								<th>Account Number</th>
								<th>IFC Code</th>
								<th>Name</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${customerbeneficiaries}" var="beneficiary">
								<tr>
									<td><c:out value="${beneficiary.getAccountNo()}" /></td>
									<td><c:out value="${beneficiary.getIFCCode()}" /></td>
									<td><c:out value="${beneficiary.getName()}" /></td>
									<td><a
										href="deletebeneficiary?action=customer&id=${beneficiary.getId()}"
										onclick="
                                                return confirm('Are you sure you want to delete this beneficiary?');">
											<i class="fa fa-fw fa-trash" aria-hidden="true"></i>
									</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</c:if>





	<!-- /.container-fluid -->

	<!-- /.content-wrapper -->

	<!-- /.content-wrapper -->

	<footer class="sticky-footer">
		<div class="container">
			<div class="text-center">
				<small>Copyright &copy; Service Secure Transaction 2017</small>
			</div>
		</div>
	</footer>

	<!-- Scroll to Top Button -->
	<a class="scroll-to-top rounded" href="#page-top"> <i
		class="fa fa-angle-up"></i>
	</a>

	<!-- Logout Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">Select "Logout" below if you are ready
					to end your current session.</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Cancel</button>
					<a class="btn btn-primary" href="logout">Logout</a>
				</div>
			</div>
		</div>
	</div>

	<script src="resources/vendor/jquery/jquery.min.js"></script>
	<script src="resources/vendor/popper/popper.min.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="resources/vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="resources/vendor/chart.js/Chart.min.js"></script>
	<script src="resources/vendor/datatables/jquery.dataTables.js"></script>
	<script src="resources/vendor/datatables/dataTables.bootstrap4.js"></script>

	<!-- Custom scripts for this template -->
	<script src="resources/js/sb-admin.min.js"></script>
	<script type="text/javascript">
		window.onload = alertName;
	</script>
</body>
</html>

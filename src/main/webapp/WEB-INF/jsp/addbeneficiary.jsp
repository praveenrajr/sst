<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<meta name="description" content="">
<meta name="author" content="">
<title>Add Beneficiary</title>

<!-- Bootstrap core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- Plugin CSS -->
<link href="resources/vendor/datatables/dataTables.bootstrap4.css"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="resources/css/sb-admin.css" rel="stylesheet">
<script type="text/javascript">
    function confSubmit(form) {
        if (confirm("Are you sure you want to delete?")) {
            form.submit();
            return true;
        } else {
            return false;
        }
    }
    
    function alertName() {
        var success = '${success}';
        var failure = '${error}';
        if (success) {
            alert(success);
            window.location="getCustomerById?action=customerview";
        }
        if (failure) {
            alert(failure);
        }
    }
</script>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top"
		id="mainNav">
		<a class="navbar-brand" href="#">Service Secure Transaction</a>
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarResponsive"
			aria-controls="navbarResponsive" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Components"><a
					class="nav-link nav-link-collapse collapsed" data-toggle="collapse"
					href="#collapseComponents" data-parent="#exampleAccordion"> <i
						class="fa fa-fw fa-link"></i> <span class="nav-link-text">
							Beneficiary</span>
				</a>
					<ul class="sidenav-second-level collapse" id="collapseComponents">
						<li><a href="viewbeneficiaries?action=beneficiary">View all</a></li>
						<li><a href="requestbeneficiary">Request</a></li>
					</ul>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
                    title="Link"><a class="nav-link" href="viewbeneficiaries?action=transaction"> <i
                        class="fa fa-fw fa-link"></i> <span class="nav-link-text">
                            Transaction</span>
                </a></li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Link"><a class="nav-link" href="getCustomerById?action=customerview"> <i
						class="fa fa-fw fa-link"></i> <span class="nav-link-text">
							Profile</span>
				</a></li>
			</ul>
			<ul class="navbar-nav sidenav-toggler">
				<li class="nav-item"><a class="nav-link text-center"
					id="sidenavToggler"> <i class="fa fa-fw fa-angle-left"></i>
				</a></li>
			</ul>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link" data-toggle="modal"
					data-target="#exampleModal"> <i class="fa fa-fw fa-sign-out"></i>
						Logout
				</a></li>
			</ul>
		</div>
	</nav>
	<div class="content-wrapper">

		<div class="container-fluid">

			<c:set var="customer" value="${account}" />
			<!-- Breadcrumbs -->
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><c:out
						value="${account.getAccountType().getType()} :" /></li>
				<li class="breadcrumb-item active">Total Available Balance INR:
					<c:out value="${account.getAmount()}" />
				</li>
			</ol>
		</div>
		<div class="container">

			<div class="card card-register mx-auto mt-5">
				<div class="card-header">Add Beneficiary</div>
				<div class="card-body">
					<form:form id="beneficiaryrequest" action="addbeneficiary"
						method="post" modelAttribute="beneficiary">
						<div class="form-group">
									<label for="exampleInputName">Name</label>
									<form:input path="name" class="form-control" id="Name"
										placeholder="Beneficiary Name"
										pattern="[A-Za-z]*" maxlength="20" required="required" />
							
						</div>
						<div class="form-group">
									<label for="exampleInputEmail1">Account Number</label>
									<form:input path="accountNo" class="form-control"
										placeholder="Account Number" pattern="[0-9]+" minlength="6" maxlength="8"
										required="required" />
						</div>
						<div class="form-group">
									<label for="exampleInputPassword1">IFCCode</label>
									<form:input path="IFCCode" class="form-control"
										id="exampleInputPassword1" placeholder="IFCCode"
										pattern="[A-Za-z0-9]+" maxlength="8" required="required" />
						</div>
						<input type = "submit" class="btn btn-primary btn-block" value ="Add">
					</form:form>
				</div>
			</div>
		</div>
	</div>

	<footer class="sticky-footer">
		<div class="container">
			<div class="text-center">
				<small>Copyright &copy; Service Secure Transaction 2017</small>
			</div>
		</div>
	</footer>

	<!-- Scroll to Top Button -->
	<a class="scroll-to-top rounded" href="#page-top"> <i
		class="fa fa-angle-up"></i>
	</a>

	<!-- Logout Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">Select "Logout" below if you are ready
					to end your current session.</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Cancel</button>
					<a class="btn btn-primary" href="logout">Logout</a>
				</div>
			</div>
		</div>
	</div>

	<!-- Bootstrap core JavaScript -->
	<script src="resources/vendor/jquery/jquery.min.js"></script>
	<script src="resources/vendor/popper/popper.min.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="resources/vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="resources/vendor/chart.js/Chart.min.js"></script>
	<script src="resources/vendor/datatables/jquery.dataTables.js"></script>
	<script src="resources/vendor/datatables/dataTables.bootstrap4.js"></script>

	<!-- Custom scripts for this template -->
	<script src="resources/js/sb-admin.min.js"></script>
	<script type="text/javascript">
         window.onload = alertName;
    </script>

</body>
</html>

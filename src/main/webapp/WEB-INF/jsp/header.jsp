<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<meta name="description" content="">
<meta name="author" content="">
<title>Welcome Admin</title>

<!-- Bootstrap core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- Plugin CSS -->
<link href="resources/vendor/datatables/dataTables.bootstrap4.css"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="resources/css/sb-admin.css" rel="stylesheet">
</head>
<body>
	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top"
		id="mainNav"> <a class="navbar-brand" href="#">Service
		Secure Transaction</a>
	<button class="navbar-toggler navbar-toggler-right" type="button"
		data-toggle="collapse" data-target="#navbarResponsive"
		aria-controls="navbarResponsive" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarResponsive">
		<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
			<li class="nav-item" data-toggle="tooltip" data-placement="right"
				title="Link"><a class="nav-link"> <i
					class="fa fa-fw fa-fw fa-user" aria-hidden="true"></i> <span
					style="font-weight: bold; color: white;"> ${name}</span>
			</a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right"
				title="Components"><a
				class="nav-link nav-link-collapse collapsed" data-toggle="collapse"
				href="#collapseComponents" data-parent="#exampleAccordion"> <i
					class="fa fa-fw fa-wrench"></i> <span class="nav-link-text">
						Requests</span>
			</a>
				<ul class="sidenav-second-level collapse" id="collapseComponents">
					<li><a href="getbeneficiaries">Benificiary Request</a></li>
					<li><a href="getCustomerRequests">Customer Request</a></li>
				</ul></li>

			<li class="nav-item" data-toggle="tooltip" data-placement="right"
				title="Link"><a href="registerAccount" class="nav-link"> <i
					class="fa fa-fw fa-plus-square" aria-hidden="true"></i> <span
					class="nav-link-text"> Create Account</span>
			</a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right"
				title="Link"><a href="getAllCustomers" class="nav-link"> <i
					class="fa fa-fw fa-eye" aria-hidden="true"></i> <span
					class="nav-link-text"> View Customers</span>
			</a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right"
				title="Link"><a href="getAllAccounts" class="nav-link"> <i
					class="fa fa-fw fa-eye" aria-hidden="true"></i> <span
					class="nav-link-text"> View Accounts</span>
			</a></li>
			<li class="nav-item" data-toggle="tooltip" data-placement="right"
				title="Link"><a href="getAllTransactions" class="nav-link"> <i
					class="fa fa-fw fa-exchange" aria-hidden="true"></i> <span
					class="nav-link-text"> Transactions</span>
			</a></li>
		</ul>
		<ul class="navbar-nav sidenav-toggler">
			<li class="nav-item"><a class="nav-link text-center"
				id="sidenavToggler"> <i class="fa fa-fw fa-angle-left"></i>
			</a></li>
		</ul>
		<ul class="navbar-nav ml-auto">

			<li class="nav-item"><span style="color: #ffffff"> </span><a
				class="nav-link" data-toggle="modal" data-target="#exampleModal"
				data-toggle="tooltip" title="deposit"> <i
					class="fa fa-fw fa-sign-out"></i> Logout
			</a></li>
		</ul>
	</div>
	</nav>
	<!-- Bootstrap core JavaScript -->
	<script src="resources/vendor/jquery/jquery.min.js"></script>
	<script src="resources/vendor/popper/popper.min.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Custom scripts for this template -->
	<script src="resources/js/sb-admin.js"></script>
	<!-- Plugin JavaScript -->
	<script src="resources/vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="resources/vendor/chart.js/Chart.min.js"></script>
	<script src="resources/vendor/datatables/jquery.dataTables.js"></script>
	<script src="resources/vendor/datatables/dataTables.bootstrap4.js"></script>

	<!-- Custom scripts for this template -->
	<script src="resources/js/sb-admin.min.js"></script>
</body>
</html>
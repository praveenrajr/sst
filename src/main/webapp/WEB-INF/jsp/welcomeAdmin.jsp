<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="header.jsp"%>
<%@ include file="footer.jsp"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<c:if test="${sessionScope['role'] != 'admin'}">
	<c:redirect url="login" />
</c:if>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<meta name="description" content="">
<meta name="author" content="">
<title>Welcome Admin</title>

<!-- Bootstrap core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- Plugin CSS -->
<link href="resources/vendor/datatables/dataTables.bootstrap4.css"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="resources/css/sb-admin.css" rel="stylesheet">
<link href="resources/css/style.css" rel="stylesheet">
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
	<div class="content-wrapper">
		<div class="container-fluid">
			<!-- Example Tables Card -->
			<div class="card mb-3">
				<c:if test="${accounts != null}">
					<div class="card-header">
						<i class="fa fa-table"></i> Account Details
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-bordered" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>Id</th>
										<th>Account</th>
										<th>Type</th>
										<th>Date Opened</th>
										<th>Amount</th>
										<th>IFCCode</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${accounts}" var="account">
										<tr>
											<td><c:out value="${account.getId()}" /></td>
											<td><a
												href="getCustomerByAccountNo?action=search&accountNo=<c:out value="${account.getAccountNo()}" />"><c:out
														value="${account.getAccountNo()}" /></a></td>
											<td><c:out value="${account.getAccountType().getType()}" /></td>
											<td><c:out value="${account.getDateOpened()}" /></td>
											<td><c:out value="${account.getAmount()}" /></td>
											<td><c:out value="${account.getIFCCode()}" /></td>
											<td><a data-toggle="tooltip" title="Delete"
												href="deleteAccountById?action=close&accountNo=<c:out value="${account.getAccountNo()}" />"
												onclick="
                                                return confirm('Are you sure you want to delete this item?');"><i
													class="fa fa-trash" aria-hidden="true"></i> </a></td>
											<td><a data-toggle="tooltip" title="Deposit"
												href="deposit?action=deposit&accountNo=<c:out value="${account.getAccountNo()}" />
                                            "><i
													class="fa fa-credit-card" aria-hidden="true"></i> </a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</c:if>
				<c:if test="${customer != null}">
					<div class="card-footer small text-muted" style="font-size: 20px;">Customer
						Details</div>
					<table class="table table-bordered" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>Id</th>
								<th>FirstName</th>
								<th>LastName</th>
								<th>Email</th>
								<th>AdhaarNo</th>
								<th>AccountNo</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${customer}" var="customer">
								<tr class='clickable-row' data-toggle="tooltip"
									title="viewPersonalDetails"
									data-href="getCustomerById?action=adminview&id=<c:out value="${customer.id}" />">
									<td><c:out value="${customer.getId()}" /></td>
									<td><c:out value="${customer.firstName}" /></td>
									<td><c:out value="${customer.lastName}" /></td>
									<td><c:out value="${customer.email}" /></td>
									<td><c:out value="${customer.adhaarNo}" /></td>
									<td><a data-toggle="tooltip" title="SearchAccount"
										href="getAccountByNumber?action=search&accountNo=<c:out value="${customer.account.accountNo}" />"><c:out
												value="${customer.account.accountNo}" /></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
				<c:if test="${customerApprovals != null}">
					<div class="card-footer small text-muted" style="font-size: 20px;">Customers
						Request :</div>
					<table class="table table-bordered" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>Id</th>
								<th>FirstName</th>
								<th>Email</th>
								<th>AccountNo</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${customerApprovals}" var="customer">
								<tr>
									<td><c:out value="${customer.getId()}" /></td>
									<td><c:out value="${customer.getFirstName()}" /></td>
									<td><c:out value="${customer.getEmail()}" /></td>
									<td><c:out value="${customer.account.accountNo}" /></td>
									<td><a data-toggle="tooltip" title="Approve"
										href="approveCustomer?action=approve&customerId=<c:out value="${customer.id}" />"><i
											class="fa fa-thumbs-o-up" aria-hidden="true"></i> </a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
				<c:if test="${beneficiaries != null}">
					<div class="card-footer small text-muted" style="font-size: 20px;">Beneficiaries
						Request</div>
					<table class="table table-bordered" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>Id</th>
								<th>AccountNo</th>
								<th>Name</th>
								<th>IFCCode</th>
								<th>Action</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${beneficiaries}" var="beneficiary">
								<tr>
									<td><c:out value="${beneficiary.getId()}" /></td>
									<td><c:out value="${beneficiary.getAccountNo()}" /></td>
									<td><c:out value="${beneficiary.getName()}" /></td>
									<td><c:out value="${beneficiary.getIFCCode()}" /></td>
									<td><a data-toggle="tooltip" title="Approve"
										href="approvebeneficiary?action=beneficary&bId=${beneficiary.getId()}"><i
											class="fa fa-thumbs-o-up" aria-hidden="true"></i> </a></td>
									<td><a data-toggle="tooltip" title="Disapprove"
										href="deletebeneficiary?action=admin&id=${beneficiary.getId()}"
										onclick="
                                                return confirm('Are you sure you want to delete this beneficiary?');">
											<i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
									</a></td>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
		</div>
		<c:if test="${transactions != null}">
			<div class="card mb-3">
				<div class="card-header">
					<i class="fa fa-table"></i> Transactions
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%"
							cellspacing="0">
							<thead>
								<tr>
									<th>Date</th>
									<th>Narration</th>
									<th>Withdrawal</th>
									<th>Deposit</th>
									<th>Closing Balance</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${transactions}" var="transaction">
									<tr>
										<td><c:out value="${transaction.getDate()}" /></td>
										<td><c:out value="${transaction.getDescription()}" /></td>
										<td><c:out value="${transaction.getWithdraw()}" /></td>
										<td><c:out value="${transaction.getDeposit()}" /></td>
										<td><c:out value="${transaction.getClosing_Balance()}" /></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</c:if>

		<c:if test="${personalDetails != null}">
			<div class="container">
				<div class="card card-register mx-auto mt-1">
					<div class="card-header">Personal Information</div>
					<div class="card-body">
						<table class="table">
							<tr>
								<td>Id</td>
								<td><c:out value="${personalDetails.getId()}" /></td>
							</tr>
							<tr>
								<td>FirstName</td>
								<td><c:out value="${personalDetails.getFirstName()}" /></td>
							</tr>

							<tr>
								<td>LastName</td>
								<td><c:out value="${personalDetails.getLastName()}" /></td>
							</tr>

							<tr>
								<td>DOB</td>
								<td><c:out value="${personalDetails.getDOB()}" /></td>
							</tr>
							<tr>
								<td>Email</td>
								<td><c:out value="${personalDetails.getEmail()}" /></td>
							</tr>
							<tr>
								<td>AdhaarNo</td>
								<td><c:out value="${personalDetails.getAdhaarNo()}" /></td>
							</tr>
							<tr>
								<td>AccountNo</td>
								<td><c:out
										value="${personalDetails.getAccount().getAccountNo()}" /></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="card card-register mx-auto mt-1">
					<div class="card-header">
						Address
					</div>
					<div class="card-body">
						<table class="table">
							<c:set var="addresses" value="${personalDetails.getAddresses()}" />
							<th>Street</th>
							<th>City</th>
							<th>State</th>
							<th>Zipcode</th>
							<c:forEach items="${addresses}" var="address">
								<tr>
									<td><c:out value="${address.getStreet()}" /></td>
									<td><c:out value="${address.getCity()}" /></td>
									<td><c:out value="${address.getState()}" /></td>
									<td><c:out value="${address.getZipcode()}" /></td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</div>
		</c:if>
	</div>


	<!-- /.container-fluid -->

	<c:if test="${success != null}">
		<jsp:include page="message.jsp" />
	</c:if>
	<c:if test="${error != null}">
		<jsp:include page="message.jsp" />
	</c:if>
	<script src="resources/vendor/jquery/jquery.min.js"></script>
	<script src="resources/vendor/popper/popper.min.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="resources/vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="resources/vendor/chart.js/Chart.min.js"></script>
	<script src="resources/vendor/datatables/jquery.dataTables.js"></script>
	<script src="resources/vendor/datatables/dataTables.bootstrap4.js"></script>

	<!-- Custom scripts for this template -->
	<script src="resources/js/sb-admin.min.js"></script>
	<script src="resources/js/validate.css"></script>
	<script>
		jQuery(document).ready(function($) {
			$(".clickable-row").click(function() {
				window.location = $(this).data("href");
			});
		});
	</script>
</body>

</html>

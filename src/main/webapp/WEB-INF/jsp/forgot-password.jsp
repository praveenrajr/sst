<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Forgot-password</title>

<link rel="stylesheet" href="resources/css/bootstrap.min.css">
<script src="resources/jquery/jquery.min.js"></script>
<script src="resources/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="resources/css/forgot.css">
<script>
	function alertName() {
		var Msg = '${message}';
		if (Msg) {
			alert(Msg);
			window.location.href = 'login';
		}
	}
</script>

</head>
<body>
	<form action="LoginController" method="post">
		<h6>Forgot your password?</h6>
		<p>
			<label for="Email" class="floatLabel">Email</label> <input id="Email"
				name="email" type="text">
		</p>
		<p>
			<label for="password" class="floatLabel">Password</label> <input
				id="password" name="newpassword" type="password"> <span>Enter
				a password longer than 8 characters</span>
		</p>
		<p>
			<label for="confirm_password" class="floatLabel">Confirm
				Password</label> <input id="confirm_password" name="confirmpassword"
				type="password"> <span>Your passwords do not match</span>
		</p>

		<button class="btn btn-primary btn-block" value="Reset"
			name="passwordreset">RESET</button><br>
		<div class="text-center">
			<a class="d-block small mt-3" href="login" style="font-size:15px;">Login Page </a>
		</div>
	</form>
	<script type="text/javascript">
		window.onload = alertName;
	</script>
	<script src="resources/js/forgot.js">
		
	</script>

	<script src="resources/js/index.js"></script>
</body>
</html>

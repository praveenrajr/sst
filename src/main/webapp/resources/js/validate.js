function checkPass() {
//Store the password field objects into variables ...
var pass1 = document.getElementById('password');
var pass2 = document.getElementById('confirmPassword');
var message = document.getElementById('confirmMessage');
var goodColor = "#66cc66";
var badColor = "#ff6666";
if (pass1.value == pass2.value) {
pass2.style.backgroundColor = goodColor;
message.style.color = goodColor;
message.innerHTML = "Passwords Match!"
} else {
pass2.style.backgroundColor = badColor;
message.style.color = badColor;
message.innerHTML = "Passwords Do Not Match!"
}
}

function ValidateContactForm() {
	var name = document.getElementById("FirstName");
	var lastname = document.getElementById("lastName");
	var email = document.getElementById("email");
	var phone = document.getElementById("phoneNo");
	var password = document.getElementById("passowrd");
	var adhaarNo = document.getElementById("adhaarNo");
	var accountNo = document.getElementById("accountNo");
	var role = document.getElementById("role");
	var male = document.getElementById("male");
	var female = document.getElementById("female");

	if (name.value == "") {
		window.alert("Please enter your first name.");
		name.focus();
		return false;
	}
	if (lastname.value == "") {
		window.alert("Please enter your last name.");
		lastname.focus();
		return false;
	}

	if (email.value == "") {
		window.alert("Please enter a valid e-mail address.");
		email.focus();
		return false;
	}
	if (email.value.indexOf("@", 0) < 0) {
		window.alert("Please enter a valid e-mail address.");
		email.focus();
		return false;
	}
	if (email.value.indexOf(".", 0) < 0) {
		window.alert("Please enter a valid e-mail address.");
		email.focus();
		return false;
	}

	if (phone.value == "") {
		window.alert("Please enter your phone number.");
		phone.focus();
		return false;
	} else if (phone.length < 10 && phone > 13) {
		window.alert("Phone number should have 10 to 13 digits.");
		phone.focus();
		return false;
	}
	if (accountNo.value == "") {
		window.alert("Please enter account number.");
		accountNo.focus();
		return false;
	}
	if (password.length < 13 && accountNO > 13) {
		window.alert("Account number should have 13 digits.");
		accountNo.focus();
		return false;
	}
	if (adhaarNo.value == "") {
		window.alert("Please enter your adhaar number.");
		adhaarNo.focus();
		return false;
	}
	if (adhaarNo.length < 14 && adhaarNo > 14) {
		window.alert("Adhaar number should have 14 digits.");
		adhaarNo.focus();
		return false;
	}
	if (password.value == "") {
		window.alert("Please enter your passowrd.");
		password.focus();
		return false;
	}
	if (password.length < 6) {
		window.alert("Password should have minimum 6 characters.");
		password.focus();
		return false;
	}
	if (role == "-1") {
		window.alert("Please select your role");
		role.focus();
		return false;
	}
	if (male.checked == false && female.checked == false) {
		window.alert("Please select gender.");
		return false
	}
	return true;
}

$(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip();
});

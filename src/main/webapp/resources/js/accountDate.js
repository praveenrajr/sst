$(function() {
	$('#datePicker').datepicker({
		format :'yyyy-mm-dd',
		autoclose : true,
		endDate : new Date()
	});
});
package com.ideas2it.sst.exception;

/**
 * Get the developer context message when the hibernate exception occur for easy
 * to debug in future.
 *
 * @author Jothiprakash created on 30/08/17.
 */
public class AppException extends Exception {

    /**
     * Receives the hibernate exception as well as the developer context message
     * for easy debug in future.
     * 
     * @param message
     *            Exception context message.
     * @param sqlException
     *            Exception occurred from database.
     */
    public AppException(String message, Throwable sqlException) {
        super(message, sqlException);
    }

    /**
     * Receives the developer context message when the sql exception is occurred
     * 
     * @param message
     *            Exception context message.
     * 
     */
    public AppException(String message) {
        super(message);
    }
}

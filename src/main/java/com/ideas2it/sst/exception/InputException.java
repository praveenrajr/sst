package com.ideas2it.sst.exception;

/**
 * It is custom exception to handle all types of exceptions.
 *
 * @author S.Kalpana created on 10/7/2017
 */
public class InputException extends Exception {

    /**
     * Class constructor without parameters.
     */
    public InputException() {
    }

    /**
     * Class constructor to get throwable cause and access the super class.
     *
     * @param cause
     *            It indicates cause of throwable.
     */
    public InputException(Throwable cause) {
        super(cause);
    }

    /**
     * Class constructor to get message and access the super class construtor.
     *
     * @param message
     *            It indicates the detailed message
     */
    public InputException(String message) {
        super(message);
    }

    /**
     * Class constructor to get message and throwable cause and access the super
     * class construtor.
     *
     * @param message
     *            It indicates the detailed message
     * @param cause
     *            It indicates cause of throwable.
     */
    public InputException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Class constructor with the specified detail message, cause, suppression
     * stack trace enabled or disabled, and writable enabled or disabled.
     * 
     * @param message
     *            It indicates the detailed message.
     * @param cause
     *            It indicates cause of throwable.
     * @param enableSuppression
     *            It indicates whether or not suppression is enabled or disabled
     * @param writableStackTrace
     *            Whether or not the stack trace should be writable
     */
    public InputException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

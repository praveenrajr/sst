/**
 * 
 */
package com.ideas2it.sst.service;

import java.util.List;

import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Transaction;

/**
 * <p>
 * It exposes the overall process of read operation with respect to transaction.
 * </p>
 * 
 * @author Kalpana S created on 08-Sep-2017
 *
 */
public interface TransactionService {

    /**
     * It returns the all available transactions from repository.
     * 
     * @return List of all available transactions.
     * @throws AppException if, could not read transactions.
     */
    List<Transaction> getAllTransactions() throws AppException;
}

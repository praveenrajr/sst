/**
 * 
 */
package com.ideas2it.sst.service;

import java.util.List;

import com.ideas2it.sst.model.Role;

/**
 * <p>
 * It exposes the overall read process of role which associates with customer.
 * </p>
 * 
 * @author Kalpana S created on 30-Aug-2017
 * 
 */
public interface RoleService {

    /**
     * Get the all available roles from the repository.
     * 
     * @return List of all roles.
     */
    List<Role> getAllRoles();
}

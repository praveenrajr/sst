package com.ideas2it.sst.service;

import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Customer;

/**
 * Authenticate the login details and give access to the records if authenticate
 * success.
 * 
 * @author Jothiprakash created on 29/08/17.
 *
 */
public interface LoginService {

    /**
     * Get the user id and password to authenticate if id and password are
     * matched it return true.
     * 
     * @param accountNo
     *            user id to check if it is exist and match with password.
     * @param password
     *            password to check if it is exist and match with userid.
     * @return boolean if accountNo and password are matched return true.
     * @throws AppException
     *             if failed to check the user id and password.
     */
    Customer loginAuthenticate(Long accountNo, String password)
            throws AppException;

    /**
     * Get the email address and new password to reset into the record
     * 
     * @param email
     *            email id to check if it is exist in the database.
     * @param newpassword
     *            to update new password for the existing customer.
     * @return String status message for reset password.
     * @throws AppException
     *             if failed to update new password.
     */
    String resetPassword(String email, String newpassword) throws AppException;
}

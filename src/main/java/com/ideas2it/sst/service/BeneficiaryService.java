package com.ideas2it.sst.service;

import java.util.List;

import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Account;
import com.ideas2it.sst.model.Beneficiary;

/**
 * It manages the business logic for beneficiaries.
 * 
 * @author Jothiprakash created on 2017-09-04.
 *
 */
public interface BeneficiaryService {

    /**
     * Get the beneficiary id and perform deletion operation for that id.
     * 
     * @param id
     *            Used to delete the beneficiary relative to id.
     * @throws AppException
     *             if not able to delete beneficiary.
     */
    void deleteBeneficiaryById(String id) throws AppException;

    /**
     * Get the beneficiary details that will be added to the record for customer
     * beneficiary
     * 
     * @param beneficiary
     *            Object contain beneficiary details.
     * @throws AppException
     *             if, could not create beneficiaries.
     */
    void addBeneficiary(Beneficiary beneficiary) throws AppException;

    /**
     * Get all the beneficiaries who are all waiting for approval by admin.
     * 
     * @return List<Beneficiary> beneficiaries waiting for approval.
     * @throws AppException
     *             if not able to retrieve the beneficiaries.
     */
    List<Beneficiary> getRequestedBeneficiaries() throws AppException;

    /**
     * Get the beneficiary id and approve the beneficiary for the customer.
     * 
     * @param id
     *            Used to approve the beneficiary relative to id.
     * @throws AppException
     *             if not able to delete beneficiary.
     */
    void approveBeneficiaryById(String id) throws AppException;

    /**
     * Takes the beneficiary id to details who matches with given id. It throws
     * AppException could not read beneficiary details.
     * 
     * @param id
     *            It denotes the unique id for each beneficiary.
     * @return Beneficiary object of persistent class.
     * @throws AppException
     *             if, could not find beneficiary details.
     */
    Beneficiary getBeneficiaryById(Long id) throws AppException;
}

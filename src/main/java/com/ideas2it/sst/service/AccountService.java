package com.ideas2it.sst.service;

import java.util.List;
import java.util.Set;

import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.exception.InputException;
import com.ideas2it.sst.model.Account;
import com.ideas2it.sst.model.AccountType;
import com.ideas2it.sst.model.Customer;
import com.ideas2it.sst.model.Transaction;

/**
 * <p>
 * It exposes the overall process of account details with CRUD operation.
 * </p>
 * 
 * @author Jothiprakash created on 31/08/17.
 *
 */
public interface AccountService {

    /**
     * It get the all available account details.
     * 
     * @return Set of all account details.
     * @throws AppException
     *             if, could not read all account details.
     */
    Set<Account> getAllAccounts() throws AppException;

    /**
     * Gets the all available account type details.
     * 
     * @return List of all available account types.
     * @throws AppException
     *             if, could not read all account types.
     */
    List<AccountType> getAllAccountTypes() throws AppException;

    /**
     * Get the account detail which match with given account number.
     * 
     * @param number
     *            It indicates the account number to find each account.
     * @return accountNo object if account number matches.
     * @throws AppException
     *             if, could not read account by number.
     */
    Account getAccountByNumber(String number) throws AppException;

    /**
     * Creates the new record for given account persistent class. If inputs are
     * invalid it throws the exception with error message to user.
     * 
     * @param account
     *            object with persistent account class.
     * @return Message about account insertion process.
     * @throws AppException
     *             if, could not insert account details.
     * @throws InputException
     *             if, errors in given input data.
     */
    String addAccountDetails(Account account)
            throws AppException, InputException;

    /**
     * It gets the customer details who matches with given account number. If
     * customer not exist in that account number it returns exception message.
     * 
     * @param accountNo
     *            Account number persistent class account.
     * @return Message about account close process.
     * @throws AppException
     *             if, cannot find customer using account number.
     */
    String closeAccountByNumber(String accountNo) throws AppException;

    /**
     * Gets account number and amount to deposit to given account. If amount
     * deposit fails it throws exception.
     * 
     * @param accountNo
     *            It denotes the account number to deposit amount.
     * @param amount
     *            It denotes the new amount to deposit to account.
     * @throws AppException
     *             if, account update process failed.
     */
    void depositAmount(String accountNo, String amount) throws AppException;

    /**
     * Used to perform the transaction for the customer
     * 
     * @param transaction
     *            Object contains transaction details with account id and
     *            beneficiary id
     * @throws AppException if transaction process failure.
     */
    void performTransaction(Transaction transaction) throws AppException;
}

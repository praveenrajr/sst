package com.ideas2it.sst.service;

import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Address;

/**
 * <p>
 * It abstracts the overall process of address create and update operations.
 * </p>
 *
 * @author Kalpana S created on 04-Sep-2017
 */
public interface AddressService {

    /**
     * Takes an address details for customer and create new record for address.
     *
     * @param address
     *            Instance of persistent class.
     * @return Message about address creation process.
     * @throws AppException
     *             if cannot add address details.
     */
    String addAddress(Address address) throws AppException;
}

package com.ideas2it.sst.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.sst.common.CommonUtil;
import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.dao.CustomerDao;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.exception.InputException;
import com.ideas2it.sst.model.Account;
import com.ideas2it.sst.model.Address;
import com.ideas2it.sst.model.Beneficiary;
import com.ideas2it.sst.model.Customer;
import com.ideas2it.sst.model.Role;
import com.ideas2it.sst.service.AccountService;
import com.ideas2it.sst.service.AddressService;
import com.ideas2it.sst.service.CustomerService;

/**
 * <p>
 * It performs the customer all business logic operations and perform validation
 * for their input details. Also it implements the CustomerService interface.
 * </p>
 * 
 * @author Kalpana S created on 29-Aug-2017
 */
@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private AccountService accountService;
    @Autowired
    private AddressService addressService;

    /**
     * @see com.ideas2it.sst.service.CustomerService#addCustomer(Customer)
     */
    public Long addCustomer(Customer customer)
            throws AppException, InputException {
        StringBuilder errors = new StringBuilder();
        Role role = new Role();
        role.setId(1);
        Account account = accountService.getAccountByNumber(
                String.valueOf(customer.getAccount().getAccountNo()));
        customer.setAccount(account);
        customer.setRole(role);
        if (validateCustomerDetails(customer, errors)) {
            return customerDao.insertCustomer(customer);
        } else {
            throw new InputException(errors.toString());
        }
    }

    /**
     * @see com.ideas2it.sst.service.CustomerService#getBeneficiaries(Long)
     */
   public Customer getBeneficieries(Long id) throws AppException {
       Customer customer = getCustomerById(id);
       if (null != customer) {
           Set<Beneficiary> beneficiaries = new HashSet<Beneficiary>();
           for (Beneficiary beneficiary : customer.getBeneficiaries()) {
               if (0 == beneficiary.getIsDeleted()
                       && 1 == beneficiary.getIsApproved()) {
                   beneficiaries.add(beneficiary);
               }
           }
           customer.setBeneficiaries(beneficiaries);
       }
       return customer;
   }

    /**
     * @see com.ideas2it.sst.service.CustomerService#getRegisteredCustomers()
     */
    public Set<Customer> getRegisteredCustomers() throws AppException {
        Set<Customer> customers = new HashSet<Customer>(
                customerDao.readRegisteredCustomers());
        return customers;
    }

    /**
     * @see com.ideas2it.sst.service.CustomerService#getCustomerById(Long)
     */
    public Customer getCustomerById(Long id) throws AppException {
        Customer customer = customerDao.getCustomerById(id);
        if (null != customer) {
            return customer;
        } else {
            throw new AppException(Constant.CUSTOMER_NOT_EXIST);
        }
    }

    /**
     * @see com.ideas2it.sst.service.CustomerService#approveCustomer(Long)
     */
    public void approveCustomer(Long id) throws AppException {
        Customer customer = getCustomerById(id);
        customer.setIsApproved(1);
        customerDao.updateCustomer(customer);
    }

    /**
     * @see com.ideas2it.sst.service.CustomerService#addAddress(Address)
     */
    public void addAddress(Address address) throws AppException {
        addressService.addAddress(address);
        Customer customer = getCustomerById(address.getCustomer().getId());
        customer.setIsDeleted(0);
        customerDao.updateCustomer(customer);
    }

    /**
     * @see com.ideas2it.sst.service.CustomerService#getAllCustomers()
     */
    public List<Customer> getAllCustomers() throws AppException {
        return customerDao.readAllCustomers();
    }

    /**
     * It validates the given user input details with correct format. If inputs
     * are in invalid format it appends the error message into string builder
     * reference. Then it returns the boolean result of validation process.
     * 
     * @param customer
     *            Object which contains all input details.
     * @param errors
     *            StringBuilder reference to holds the error input messages.
     * @return true, if input validation success, else false.
     */
    private boolean validateCustomerDetails(Customer customer,
            StringBuilder errors) {
        boolean success = true;
        if (!(CommonUtil.isAlphabets(customer.getFirstName()))) {
            errors.append("\n" + Constant.INVALID_NAME);
            success = false;
        }
        if (!(CommonUtil.isAlphabets(customer.getLastName()))) {
            errors.append("\n" + Constant.INVALID_NAME);
            success = false;
        }
        if (!(CommonUtil.isDigits(customer.getPhoneNo()))) {
            errors.append("\n" + Constant.INVALID_PHONENO);
            success = false;
        }
        if (!CommonUtil.isValidEmail(customer.getEmail())) {
            errors.append("\n" + Constant.INVALID_EMAILID);
            success = false;
        }
        if (!(CommonUtil.isDigits(String.valueOf(customer.getAdhaarNo())))) {
            errors.append("\n" + Constant.INVALID_ADHAARNO);
            success = false;
        }
        if (!(CommonUtil.isDigits(
                String.valueOf(customer.getAccount().getAccountNo())))) {
            errors.append("\n" + Constant.INVALID_ACCOUNTNO);
            success = false;
        }
        return success;
    }
}

package com.ideas2it.sst.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.sst.common.CommonUtil;
import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.dao.AccountDao;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.exception.InputException;
import com.ideas2it.sst.model.Account;
import com.ideas2it.sst.model.AccountType;
import com.ideas2it.sst.model.Beneficiary;
import com.ideas2it.sst.model.Customer;
import com.ideas2it.sst.model.Transaction;
import com.ideas2it.sst.service.AccountService;

/**
 * <p>
 * It performs the all business logic operations belongs to the account entity.
 * Also implements the AccounService interface to perform its functionalities.
 * </p>
 * 
 * @author Kalpana S created on 31/08/17.
 *
 */
@Service("accountService")
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDao accountDao;

    /**
     * @see com.ideas2it.sst.service.AccountService#getAllAccounts()
     */
    public Set<Account> getAllAccounts() throws AppException {
        Set<Account> accounts = new HashSet<Account>(
                accountDao.readAllAccounts());
        return accounts;
    }

    /**
     * @see com.ideas2it.sst.service.AccountService#getAllAccountTypes()
     */
    public List<AccountType> getAllAccountTypes() throws AppException {
        return accountDao.readAllAccountTypes();
    }

    /**
     * @see com.ideas2it.sst.service.AccountService
     *      #getAccountByNumber(com.ideas2it.sst.model.Account)
     */
    public Account getAccountByNumber(String number) throws AppException {
        if (CommonUtil.isDigits(number)) {
            Account account = accountDao
                    .readAccountByNumber(Long.parseLong(number));
            if (null != account) {
                return account;
            }
            throw new AppException(Constant.ACCOUNT_NOT_EXIST + number);
        }
        throw new AppException(Constant.INVALID_ACCOUNT + number);
    }

    /**
     * @see com.ideas2it.sst.service.AccountService#addAccountDetails(Account)
     */
    public String addAccountDetails(Account account)
            throws AppException, InputException {
        StringBuilder errors = new StringBuilder();
        Long min = (long) 100000;
        Long max = (long) 100000000;
        Long randomNum = ThreadLocalRandom.current().nextLong(min, max + 1);
        account.setAccountNo(randomNum);
        if (validAccountData(account, errors)) {
            accountDao.insertAccount(account);
            return Constant.INSERT_SUCCESS;
        } else {
            throw new InputException(errors.toString());
        }
    }

    /**
     * @see com.ideas2it.sst.service.AccountService#depositAmount(String,
     *      String)
     */
    public void depositAmount(String accountNo, String amount)
            throws AppException {
        Account account = getAccountByNumber(accountNo);
        Long newAmount = Long.parseLong(amount);
        System.out.println(newAmount);
        account.setAmount(account.getAmount() + newAmount);
        accountDao.updateAccount(account);
    }

    /**
     * non-javadoc
     * 
     * @see com.ideas2it.sst.service.AccountService#performTransaction(Transaction)
     */
    public void performTransaction(Transaction transaction)
            throws AppException {
        Set<Transaction> transactions = null;
        if (transaction.getAccount().getAmount() > transaction.getWithdraw()) {
            transactions = new HashSet<>();
            transaction.getAccount()
                    .setAmount(transaction.getAccount().getAmount()
                            - transaction.getWithdraw());
            transaction
                    .setClosing_Balance(transaction.getAccount().getAmount());
            transactions.add(transaction);
            Account account = transaction.getAccount();
            account.setTransactions(transactions);
            accountDao.updateAccount(account);
            if (transaction.getBeneficiary().getIFCCode()
                    .equals(Constant.IFCCODE)) {
                updateBeneficiaryAccount(transaction, account, transactions);
            }
        } else {
            throw new AppException("Account balance insufficient to transfer");
        }
    }

    /**
     * @see com.ideas2it.sst.service.AccountService#closeAccountByNumber(String)
     */
    public String closeAccountByNumber(String accountNo) throws AppException {
        if (CommonUtil.isDigits(accountNo)) {
            Account account = getAccountByNumber(accountNo);
            account.getCustomer().setIsDeleted(1);
            account.setIsClosed(1);
            accountDao.updateAccount(account);
            return Constant.DELETE_SUCCESS;
        }
        return Constant.INVALID_ACCOUNT;
    }

    /**
     * @see com.ideas2it.sst.service.AccountService#updateAccount(Account)
     */
    public void updateAccount(Account account) throws AppException {
        accountDao.updateAccount(account);
        ;
    }

    /**
     * It updates the beneficiary account detail who matches with IFCCode.
     * 
     * @param transaction
     *            It denotes the transaction reference from the client request.
     * @param account
     *            It denotes the account reference for particular beneficiary.
     * @param transactions
     *            It indicates the transactions set which holds by the account.
     * @throws AppException
     *             if, could not update account.
     */
    private void updateBeneficiaryAccount(Transaction transaction,
            Account account, Set<Transaction> transactions)
            throws AppException {
        Transaction newTransaction = new Transaction();
        transactions = new HashSet<>();
        account = getAccountByNumber(String.valueOf(
                transaction.getBeneficiary().getAccountNo()));
        account.setAmount(account.getAmount() + transaction.getWithdraw());
        newTransaction.setClosing_Balance(account.getAmount());
        newTransaction.setDeposit(transaction.getWithdraw());
        newTransaction.setDescription(transaction.getDescription());
        newTransaction.setAccount(account);
        newTransaction.setDate(transaction.getDate());
        newTransaction.setFromAccount(transaction.getAccount().getAccountNo());
        transactions.add(newTransaction);
        account.setTransactions(transactions);
        accountDao.updateAccount(account);
    }

    /**
     * This method is used for validation of given input details to check with
     * correct format. If inputs are invalid it appends the error message with
     * string builder reference and returns false. Else returns true.
     * 
     * @param account
     *            Object of persistent class.
     * @param errors
     *            StringBuilder object to append error messages.
     * @throws AppException
     *             if, given input details are invalid format.
     * 
     */
    private boolean validAccountData(Account account, StringBuilder errors)
            throws AppException {
        boolean success = true;
        if (!(CommonUtil.isDigits(String.valueOf(account.getAccountNo())))) {
            errors.append("\n" + Constant.INVALID_ACCOUNTNO);
            success = false;
        }
        if (!(CommonUtil
                .isValidDate(String.valueOf(account.getDateOpened())))) {
            errors.append("\n" + Constant.INVALID_DATE);
            success = false;
        }
        if (!(CommonUtil.isDigits(String.valueOf(account.getAmount())))) {
            errors.append("\n" + Constant.INVALID_COST);
            success = false;
        }
        return success;
    }
}

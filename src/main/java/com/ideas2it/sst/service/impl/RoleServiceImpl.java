package com.ideas2it.sst.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.sst.dao.RoleDao;
import com.ideas2it.sst.model.Role;
import com.ideas2it.sst.service.RoleService;

/**
 * <p>
 * It performs the business logic operations belongs to the role model.
 * 
 * @author Kalpana S created on 30-Aug-2017
 * 
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    /**
     * @see com.ideas2it.sst.service.RoleService#getAllRoles()
     */
    public List<Role> getAllRoles() {
        return roleDao.readRoles();
    }
}

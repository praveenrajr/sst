package com.ideas2it.sst.service.impl;

import com.ideas2it.sst.common.Constant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.sst.common.CommonUtil;
import com.ideas2it.sst.dao.AddressDao;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Address;
import com.ideas2it.sst.service.AddressService;

/**
 * <p>
 * It performs the all business logic operations with respect to address
 * details. It implements the AddressService interface and overrides the all
 * methods in that interface.
 * </p>
 *
 * @author Kalpana S created on 04-Sep-2017
 */
@Service("addressService")
public class AddressServiceImpl implements AddressService {
    @Autowired
    private AddressDao addressDao;

    /**
     * non-javadoc
     *
     * @see com.ideas2it.sst.service.AddressService #addAddress(Address)
     * 
     */
    @Override
    public String addAddress(Address address) throws AppException {
        if (Constant.ZERO != addressDao.createAddress(address)) {
            return Constant.INSERT_SUCCESS;
        }
        return Constant.INSERT_FAILED;
    }
}

package com.ideas2it.sst.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.dao.LoginDao;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Customer;
import com.ideas2it.sst.service.LoginService;

/**
 * Used to authenticate the user account number and password are matched and
 * return the boolean status.
 * 
 * @author Jothiprakash created on 29/08/17
 *
 */
@Service("loginService")
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginDao loginDao;

    /**
     * non-javadoc
     *
     * @see com.ideas2it.sst.service.LoginService#loginAuthenticate(Long,String)
     */
    public Customer loginAuthenticate(Long accountNo, String password)
            throws AppException {
        return loginDao.loginAuthenticate(accountNo, password);
    }

    /**
     * non-Javadoc
     *
     * @see com.ideas2it.sst.service.LoginService#resetPassword(String, String)
     */
    public String resetPassword(String email, String newpassword)
            throws AppException {
        Customer customer = loginDao.getCustomerByEmail(email);
        if (null != customer) {
            customer.setPassword(newpassword);
            loginDao.updatePassword(customer);
            return Constant.UPDATED_SUCCESS;
        } else {
            return Constant.INVALID_EMAILID;
        }
    }
}

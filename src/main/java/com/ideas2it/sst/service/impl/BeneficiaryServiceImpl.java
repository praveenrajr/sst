/**
 * 
 */
package com.ideas2it.sst.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.dao.BeneficiaryDao;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Beneficiary;
import com.ideas2it.sst.service.BeneficiaryService;

/**
 * Perform the business logics for the beneficiary and implements the
 * BeneficiaryService interface to perform its functionalities.
 * 
 * @author Jothiprakash created on 2017-09-04.
 *
 */
@Service("beneficiaryService")
public class BeneficiaryServiceImpl implements BeneficiaryService {

    @Autowired
    private BeneficiaryDao beneficiaryDao;

    /**
     * non-javadoc
     * 
     * @see com.ideas2it.sst.service.BeneficiaryService#deleteBeneficiaryById(String)
     */
    public void deleteBeneficiaryById(String id) throws AppException {
        Beneficiary beneficiary = beneficiaryDao
                .getBeneficiaryById(Long.parseLong(id));
        beneficiary.setIsDeleted(1);
        beneficiaryDao.updateBeneficiaryById(beneficiary);
    }

    /**
     * non-javadoc
     * 
     * @see com.ideas2it.sst.service.BeneficiaryService#addBeneficiary(com.ideas2it.sst.model.Beneficiary)
     */
    public void addBeneficiary(Beneficiary beneficiary) throws AppException {
        beneficiaryDao.saveBeneficiary(beneficiary);
    }

    /**
     * non-javadoc
     * 
     * @see com.ideas2it.sst.service.BeneficiaryService#getRequestedBeneficiaries()
     */
    public List<Beneficiary> getRequestedBeneficiaries() throws AppException {
        return beneficiaryDao.getRequestedBeneficiaries();
    }

    /**
     * non-javadoc
     * 
     * @see com.ideas2it.sst.service.BeneficiaryService#approveBeneficiaryById(String)
     */
    public void approveBeneficiaryById(String id) throws AppException {
        Beneficiary beneficiary = beneficiaryDao
                .getBeneficiaryById(Long.parseLong(id));
        beneficiary.setIsApproved(1);
        beneficiaryDao.updateBeneficiaryById(beneficiary);
    }

    /**
     * non-Javadoc
     * 
     * @see com.ideas2it.sst.service.BenificiaryService#getBeneficiaryById(Long)
     */
    public Beneficiary getBeneficiaryById(Long id) throws AppException {
        return beneficiaryDao.getBeneficiaryById(id);
    }
}

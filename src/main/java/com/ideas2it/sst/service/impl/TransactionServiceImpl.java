/**
 * 
 */
package com.ideas2it.sst.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.sst.dao.TransactionDao;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Transaction;
import com.ideas2it.sst.service.TransactionService;

/**
 * @author Kalpana S created on 08-Sep-2017
 *
 */
@Service("transactionService")
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionDao transactionDao;

    /*
     * (non-Javadoc)
     * @see com.ideas2it.sst.service.TransactionService#getAllTransactions()
     */
    public List<Transaction> getAllTransactions() throws AppException {
        return transactionDao.readAllTransactions();
    }
}

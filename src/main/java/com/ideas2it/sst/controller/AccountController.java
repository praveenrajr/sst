package com.ideas2it.sst.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.common.SSTLog;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.exception.InputException;
import com.ideas2it.sst.model.Account;
import com.ideas2it.sst.model.Customer;
import com.ideas2it.sst.model.Transaction;
import com.ideas2it.sst.service.AccountService;
import com.ideas2it.sst.service.BeneficiaryService;

/**
 * <p>
 * It manages the flow of account details in online banking. Receives the
 * request and send the response to particular clients.
 * </p>
 * 
 * @author Kalpana S created on 31/08/17.
 *
 */
@Controller
public class AccountController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private BeneficiaryService beneficiaryService;

    /**
     * It creates the new record for given account details based on client
     * request and returns the response message to appropriate view.
     * 
     * @param request
     *            object which contains the request from the client.
     * @return ModelAndView object with appropriate view name and object.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     */
    @RequestMapping("/registerAccount")
    public ModelAndView createAccount(HttpServletRequest request)
            throws ServletException, IOException {
        ModelAndView modelView = new ModelAndView(Constant.ADDACCOUNT);
        try {
            modelView.addObject(Constant.ACTYPE,
                    accountService.getAllAccountTypes());
        } catch (AppException e) {
            modelView.addObject(Constant.ERROR, e.getMessage());
            SSTLog.Log(e);
        }
        modelView.addObject(Constant.ACCOUNT, new Account());
        return modelView;
    }

    /**
     * Gets the all available account details.
     * 
     * @return ModelAndView object with view name to display and object.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     */
    @RequestMapping("/getAllAccounts")
    public ModelAndView getAllAccounts() throws ServletException, IOException {
        ModelAndView modelView = new ModelAndView(Constant.WELCOMEADMIN);
        try {
            modelView.addObject(Constant.ACCOUNTS,
                    accountService.getAllAccounts());
        } catch (AppException e) {
            modelView.addObject(Constant.ERROR,
                    Constant.ACCOUNT_READ_USR_MESSAGE);
            SSTLog.Log(e);
        }
        return modelView;
    }

    /**
     * It creates the new account record from the view and return to appropriate
     * view.
     * 
     * @param account
     *            ModelAttribute of account persistent class.
     * @param request
     *            HttpServletRequest from the client.
     * @return ModelAndView object with view name to display and object.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     * 
     */
    @SuppressWarnings("finally")
    @RequestMapping(value = "/addAccount", method = RequestMethod.POST)
    public ModelAndView addAccount(
            @ModelAttribute(Constant.ACCOUNT) Account account,
            HttpServletRequest request) throws ServletException, IOException {
        ModelAndView modelView = new ModelAndView(Constant.WELCOMEADMIN);
        try {
            modelView.addObject(Constant.SUCCESS,
                    accountService.addAccountDetails(account));
            modelView.addObject(Constant.ACCOUNTS,
                    accountService.getAllAccounts());
        } catch (AppException | InputException e) {
            modelView.addObject(Constant.ERROR, e.getMessage());
            modelView.addObject(Constant.ACTYPE,
                    accountService.getAllAccountTypes());
            modelView.setViewName(Constant.ADDACCOUNT);
            SSTLog.Log(e);
        } finally {
            return modelView;
        }
    }

    @RequestMapping(value = "/addAccount", method = RequestMethod.GET)
    public void createAccounts(HttpServletRequest request) {
        System.out.println("sucess");
    }

    /**
     * It gets the particular account details which matches with given account
     * number if account exist. Else it returns exception message.
     * 
     * @param request
     *            HttpServletRequest from the client to server.
     * @return ModelAndView object with view name and model object.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     * 
     */
    @RequestMapping("/getAccountByNumber")
    public ModelAndView getAccountByNumber(HttpServletRequest request)
            throws ServletException, IOException {
        ModelAndView modelAndView = new ModelAndView(Constant.WELCOMEADMIN);
        Set<Account> accounts = new HashSet<>();
        try {
            accounts.add(accountService.getAccountByNumber(
                    request.getParameter(Constant.ACCOUNTNO)));
            modelAndView.addObject(Constant.ACCOUNTS, accounts);
        } catch (AppException e) {
            modelAndView.addObject(Constant.ERROR, e.getMessage());
            modelAndView.setViewName(Constant.WELCOMEADMIN);
            SSTLog.Log(e);
        }
        return modelAndView;
    }

    /**
     * It gets the request from the client to read customer details using their
     * account number.
     * 
     * @param request
     *            object which contains the account number to find customer.
     * @return ModelAndView object with view name and model object.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     *
     */
    @RequestMapping("/getCustomerByAccountNo")
    public ModelAndView getCustomerByAccountNo(HttpServletRequest request)
            throws ServletException, IOException {
        ModelAndView modelAndView = new ModelAndView(Constant.WELCOMEADMIN);
        Set<Customer> customer = new HashSet<>();
        try {
            Account account = accountService.getAccountByNumber(
                    request.getParameter(Constant.ACCOUNTNO));
            if (null != account.getCustomer()) {
                customer.add(account.getCustomer());
                modelAndView.addObject(Constant.CUSTOMER, customer);
            } else {
                modelAndView.addObject(Constant.ERROR,
                        Constant.CUSTOMER_NOT_EXIST);
            }
        } catch (AppException e) {
            modelAndView.addObject(Constant.ERROR, e.getMessage());
            SSTLog.Log(e);
        }
        return modelAndView;
    }

    /**
     * Gets the request to close the account details using their account number
     * and returns the response message with the model and view object.
     * 
     * @param request
     *            HttpServletRequest object to close the account details.
     * @return ModelAndView object with view name and model object.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     * 
     */
    @RequestMapping("/deleteAccountById")
    public ModelAndView closeAccountBtNumber(HttpServletRequest request)
            throws ServletException, IOException {
        ModelAndView modelAndView = new ModelAndView(Constant.WELCOMEADMIN);
        try {
            modelAndView.addObject(Constant.SUCCESS,
                    accountService.closeAccountByNumber(
                            request.getParameter(Constant.ACCOUNTNO)));
            modelAndView.addObject(Constant.ACCOUNTS,
                    accountService.getAllAccounts());
        } catch (AppException e) {
            modelAndView.addObject(Constant.SUCCESS, e.getMessage());
            SSTLog.Log(e);
        }
        return modelAndView;
    }

    /**
     * Gets the request to deposit the amount from client to particular account
     * and sends the response message to particular view.
     * 
     * @param request
     *            Request message from the client.
     * @return ModelAndView object with view name and particular object to
     *         return.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     * 
     */
    @RequestMapping("/depositAmount")
    public ModelAndView depositAmount(HttpServletRequest request)
            throws IOException, ServletException {
        ModelAndView modelView = new ModelAndView(Constant.WELCOMEADMIN);
        try {
            accountService.depositAmount(
                    request.getParameter(Constant.ACCOUNTNO),
                    request.getParameter(Constant.AMOUNT));
            modelView.addObject(Constant.SUCCESS, Constant.DEPOSIT_SUCCESS);
        } catch (AppException e) {
            modelView.addObject(Constant.ERROR, e.getMessage());
            SSTLog.Log(e);
        }
        return modelView;
    }

    /**
     * It get the transaction object contains transaction details along with
     * beneficiary id to perform transaction
     * 
     * @param transaction
     *            Object contains transaction details.
     * @param request
     *            Request message from the client.
     * @return ModelAndView object with view name and particular object to
     *         return.
     */
    @RequestMapping(value = "/transaction", method = RequestMethod.POST)
    public ModelAndView transaction(
            @ModelAttribute("transaction") Transaction transaction,
            HttpServletRequest request) {
        HttpSession session = request.getSession();
        String accountNo = (String) session.getAttribute(Constant.ACCOUNTNO);
        ModelAndView modelAndView = new ModelAndView(Constant.WELCOME);
        Account account = null;
        try {
            DateFormat format = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
            Date date = new Date();
            account = accountService.getAccountByNumber(accountNo);
            transaction.setBeneficiary(beneficiaryService
                    .getBeneficiaryById(transaction.getBeneficiary().getId()));
            transaction.setAccount(account);
            transaction.setDate(format.format(date));
            accountService.performTransaction(transaction);
            modelAndView.addObject(Constant.SUCCESS,
                    Constant.AMOUNT_TRANSFER_SUCCESSFULLY);
            modelAndView.addObject(Constant.CUSTOMER, account.getCustomer());
        } catch (AppException e) {
            modelAndView.addObject(Constant.ERROR, e.getMessage());
            modelAndView.addObject(Constant.CUSTOMER, account.getCustomer());
        }
        return modelAndView;
    }
}

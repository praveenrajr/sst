package com.ideas2it.sst.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.sst.common.Constant;

/**
 * Used to invalidate the session for the customer and navigate to login page.
 * 
 * @author Jothiprakash created on 2017-09-02.
 *
 */
@Controller
public class LogoutController {

    /**
     * Invalidate the session for the customer and navigate to login page.
     * 
     * @param request
     *            the ServletRequest object that contains the customer request
     * @param response
     *            the ServletResponse object that contains the servlet's
     *            response
     * @throws ServletException
     *             if an exception occurs that interferes with the servlet's
     *             normal operation
     * @throws IOException
     *             if an input or output exception occurs
     */
    @RequestMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        session.invalidate();
        response.sendRedirect("login");
    }
}

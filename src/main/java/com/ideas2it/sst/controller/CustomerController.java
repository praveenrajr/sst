package com.ideas2it.sst.controller;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.TransactionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.common.SSTLog;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.exception.InputException;
import com.ideas2it.sst.model.Account;
import com.ideas2it.sst.model.Address;
import com.ideas2it.sst.model.Beneficiary;
import com.ideas2it.sst.model.Customer;
import com.ideas2it.sst.model.Transaction;
import com.ideas2it.sst.service.AccountService;
import com.ideas2it.sst.service.CustomerService;
import com.ideas2it.sst.service.RoleService;

/**
 * It manages the customer flow application to receive requests and returns back
 * the response.
 * 
 * @author Kalpana S created on 29-Aug-2017
 * 
 */
@Controller
public class CustomerController {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private AccountService accountService;

    /**
     * It creates a new customer object and redirects to particular view using
     * model and view .
     * 
     * @param request
     *            HttpServletRequest from the client to process.
     * @return ModelAndview object with view name and model object.
     */
    @RequestMapping("/createCustomer")
    public ModelAndView add(HttpServletRequest request) {
        ModelAndView modelView = new ModelAndView(Constant.REGISTER);
        modelView.addObject(Constant.CUSTOMER, new Customer());
        return modelView;
    }

    /**
     * It creates the new record for customer details from the view and sends to
     * the next layer for further process.
     * 
     * @param customer
     *            model attribute from view.
     * @return ModelAndView object with view name and object.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView addCustomer(
            @ModelAttribute(Constant.CUSTOMER) Customer customer)
            throws ServletException, IOException {
        ModelAndView modelView = new ModelAndView(Constant.LOGIN);
        try {
            modelView.addObject(Constant.ID,
                    customerService.addCustomer(customer));
            modelView.addObject(Constant.SUCCESS, Constant.REGISTER_SUCCESS);
        } catch (AppException | InputException e) {
            modelView.addObject(Constant.ERROR, e.getMessage());
            modelView.setViewName(Constant.REGISTER);
            SSTLog.Log(e);
        }
        return modelView;
    }

    /**
     * Get the account details by account number.And show the details to the
     * customer.
     * 
     * @param request
     *            the ServletRequest object that contains the customer request.
     * @param response
     *            the ServletResponse object that contains the servlet's
     *            response
     * @return ModelAndView corresponding jsp page with relative details.
     * @throws ServletException
     *             if an exception occurs that interferes with the servlet's
     *             normal operation
     * @throws IOException
     *             if an input or output exception occurs.
     */
    @RequestMapping("/viewtransactions")
    public ModelAndView getTransactionsByAccNo(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        ModelAndView modelAndView = null;
        HttpSession session = request.getSession();
        String accountNo = (String) session.getAttribute(Constant.ACCOUNTNO);
        try {
            Account account = accountService.getAccountByNumber(accountNo);
            modelAndView = new ModelAndView(Constant.WELCOME, Constant.ACCOUNT,
                    account);
            modelAndView.addObject(Constant.CUSTOMER, account.getCustomer());
        } catch (AppException e) {
            SSTLog.Log(e);
        }
        return modelAndView;
    }

    /**
     * Gets the request to view all beneficiaries using particular customer.
     * Then sends the response to particular view.
     * 
     * @param request
     *            HttpSrevletRequest which holds the request object from client.
     * @param response
     *            servlet response to the client from server.
     * @return ModelAndView object with view name and particular object to that
     *         view.
     * @throws ServletException
     *             if, it affects servlet normal operations.
     * @throws IOException
     *             if an input or output exception occurs.
     */
    @RequestMapping("/viewbeneficiaries")
    public ModelAndView getBeneficiariesByAccNo(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        ModelAndView modelAndView = new ModelAndView(Constant.WELCOME);
        HttpSession session = request.getSession();
        Long id = (Long) session.getAttribute(Constant.ID);
        try {
            Customer customer = customerService.getBeneficieries(id);
            if (request.getParameter("action").equals("transaction")) {
                modelAndView.addObject(Constant.BENEFICIARIES,
                        customer.getBeneficiaries());
                modelAndView.addObject(Constant.CUSTOMER, customer);
                modelAndView.addObject(Constant.TRANSACTION, new Transaction());
            }
            if (request.getParameter("action").equals("beneficiary")) {
                modelAndView.addObject(Constant.CUSTOMER, customer);
                modelAndView.addObject(Constant.CUSTOMER_BENEFICIARIES,
                        customer.getBeneficiaries());
            }
        } catch (AppException e) {
            SSTLog.Log(e);
        }
        return modelAndView;
    }

    /**
     * Get the request to create address details for customer from the client
     * and sends the response to appropriate client.
     * 
     * @param address
     *            Address model object which holds the address details for
     *            customer.
     * @return ModelAndView object with view name to redirect and object with
     *         required message
     * @throws ServletException
     *             if, it affects servlet normal operations.
     * @throws IOException
     *             if an input or output exception occurs.
     */
    @RequestMapping("/addAddress")
    public ModelAndView addAddress(@ModelAttribute("address") Address address,
            HttpServletRequest request) throws ServletException, IOException {
        ModelAndView modelView = new ModelAndView(Constant.WELCOME);
        try {
            HttpSession session = request.getSession();
            Long id = (Long) session.getAttribute(Constant.ID);
            customerService.addAddress(address);
            modelView.addObject("successAddress", Constant.REGISTER_SUCCESS);
        } catch (AppException e) {
            e.printStackTrace();
            modelView.addObject(Constant.ERROR, Constant.ADDRESS_INSERT_FAILED);
            SSTLog.Log(e);
        }
        return modelView;
    }

    /**
     * Get the request to create a new address record for particular customer.
     * Then it sends the status message to given view name.
     * 
     * @param request
     *            HttpServletRequest from the client.
     * @return ModelAndView object with view name to redirect and appropriate
     *         object details to that view.
     */
    @RequestMapping("/createAddress")
    public ModelAndView createAddress(HttpServletRequest request) {
        ModelAndView modelView = new ModelAndView(Constant.WELCOME);
        HttpSession session = request.getSession();
        Long id = (Long) session.getAttribute(Constant.ID);
        try {
            Customer customer = customerService.getCustomerById(id);
            Address address = new Address();
            address.setCustomer(customer);
            modelView.addObject(Constant.CUSTOMER, customer);
            modelView.addObject(Constant.ADDRESS, address);
        } catch (AppException e) {
            SSTLog.Log(e);
        }
        return modelView;
    }

    /**
     * Gets the request to list all registered customers for online banking and
     * returns the object to appropriate view.
     * 
     * @param request
     *            It holds the request client request.
     * @return ModelAndView object with view name and object to display.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     * 
     */
    @RequestMapping("/getCustomerRequests")
    public ModelAndView getCustomerRequests(HttpServletRequest request)
            throws IOException, ServletException {
        ModelAndView modelView = new ModelAndView(Constant.WELCOMEADMIN);
        try {
            modelView.addObject(Constant.CUSTOMERAPPROVALS,
                    customerService.getRegisteredCustomers());
        } catch (AppException e) {
            modelView.addObject(Constant.ERROR, e.getMessage());
            SSTLog.Log(e);
        }
        return modelView;
    }

    /**
     * It gets the request to approve particular customer for online banking
     * registration.
     * 
     * @param request
     *            It indicates the request from the client to approve.
     * @return ModelAndView object with view name to return and particular
     *         object to return.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     */
    @RequestMapping("/approveCustomer")
    public ModelAndView approveCustomer(HttpServletRequest request)
            throws ServletException, IOException {
        ModelAndView modelView = new ModelAndView(Constant.WELCOMEADMIN);
        try {
            customerService.approveCustomer(
                    Long.parseLong(request.getParameter(Constant.CUSTOMER_ID)));
            modelView.addObject(Constant.SUCCESS, Constant.APPROVED_SUCCESS);
        } catch (AppException e) {
            modelView.addObject(Constant.ERROR, e.getMessage());
            SSTLog.Log(e);
        }
        return modelView;
    }

    /**
     * It is called by client to view all available customers. Then it sends the
     * result to appropriate view. If could not read all customer details it
     * returns the error message to view.
     * 
     * @return ModelAndView object with view name and object to view.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     */
    @RequestMapping("/getAllCustomers")
    public ModelAndView getAllCustomers() throws ServletException, IOException {
        ModelAndView modelView = new ModelAndView(Constant.WELCOMEADMIN);
        try {
            modelView.addObject(Constant.CUSTOMER,
                    customerService.getAllCustomers());
        } catch (AppException e) {
            modelView.addObject(Constant.ERROR, e.getMessage());
            SSTLog.Log(e);
        }
        return modelView;
    }

    /**
     * It is called by client to get particular customer using their id value.
     * Then it sends back the response message to particular view.
     * 
     * @param request
     *            HttpServletRequest from the client.
     * @return ModelAndView object with view name to return and object to that
     *         view.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     */
    @RequestMapping("/getCustomerById")
    public ModelAndView getCustomerById(HttpServletRequest request) {
        ModelAndView modelView = null;
        try {
            Customer customer = null;
            if (request.getParameter("action").equals("customerview")) {
                HttpSession session = request.getSession();
                Long id = (Long) session.getAttribute(Constant.ID);
                customer = customerService.getCustomerById(id);
                modelView = new ModelAndView(Constant.WELCOME);
                modelView.addObject(Constant.CUSTOMER, customer);
            } else {
                customer = customerService.getCustomerById(
                        Long.valueOf(request.getParameter(Constant.ID)));
                modelView = new ModelAndView(Constant.WELCOMEADMIN);
            }
            modelView.addObject(Constant.PERSONALDETAILS, customer);
        } catch (AppException e) {
            modelView.addObject(Constant.ERROR, e.getMessage());
            SSTLog.Log(e);
        }
        return modelView;
    }
}

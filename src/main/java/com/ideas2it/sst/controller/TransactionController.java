/**
 * 
 */
package com.ideas2it.sst.controller;

import java.io.IOException;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.common.SSTLog;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.service.TransactionService;

/**
 * It manages the each client request and sends the each response to particluar
 * client with respect to transaction process.
 * 
 * @author Kalpana S created on 06-Sep-2017
 *
 */
@Controller
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    /**
     * It gets the request from the client to read all transactions to view for
     * admin.
     * 
     * @return ModelAndView object with view name and list of transaction
     *         transactions to send.
     * @throws ServletException
     *             if, it affects servlet normal operations.
     * @throws IOException
     *             if an input or output exception occurs.
     */
    @RequestMapping("/getAllTransactions")
    public ModelAndView getAllTransactions()
            throws ServletException, IOException {
        ModelAndView modelView = new ModelAndView(Constant.WELCOMEADMIN);
        try {
            modelView.addObject("transactions",
                    transactionService.getAllTransactions());
        } catch (AppException e) {
            SSTLog.Log(e);
        }
        return modelView;
    }
}

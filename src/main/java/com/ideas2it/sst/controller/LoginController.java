package com.ideas2it.sst.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.common.SSTLog;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Customer;
import com.ideas2it.sst.service.AccountService;
import com.ideas2it.sst.service.LoginService;

/**
 * Authenticate the request if authentication success redirect to welcome page
 * else redirect to same page.
 * 
 * @author Jothiprakash created on 29/08/17.
 *
 */
@Controller
public class LoginController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private AccountService accountService;

    /**
     * Get the account number and password from the user to authenticate if
     * account no and password are matched create the the session and redirect
     * to the welcome page else redirect to login page.
     * 
     * @param request
     *            the ServletRequest object that contains the customer request
     * @param response
     *            the ServletResponse object that contains the servlet's
     *            response
     * @return ModelAndView corresponding jsp page with status message.
     * @throws ServletException
     *             if an exception occurs that interferes with the servlet's
     *             normal operation
     * @throws IOException
     *             if an input or output exception occurs
     */
    @RequestMapping(params = "login")
    public ModelAndView loginAuthenticate(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        String accountNo = request.getParameter(Constant.ACCOUNTNO);
        String password = request.getParameter(Constant.PASSWORD);
        ModelAndView modelAndView = null;
        try {
            Customer customer = loginService
                    .loginAuthenticate(Long.parseLong(accountNo), password);
            if (null != customer) {
                if (customer.getRole().getRole().equals(Constant.CUSTOMER)) {
                    HttpSession session = request.getSession();
                    session.setAttribute(Constant.ACCOUNTNO, accountNo);
                    session.setAttribute(Constant.ID, customer.getId());
                    session.setAttribute(Constant.NAME,
                            customer.getFirstName());
                    session.setAttribute(Constant.ROLE,
                            customer.getRole().getRole());
                    modelAndView = new ModelAndView(Constant.WELCOME,
                            Constant.CUSTOMER, customer);
                } else {
                    modelAndView = new ModelAndView(Constant.WELCOMEADMIN);
                    HttpSession session = request.getSession();
                    session.setAttribute(Constant.ACCOUNTNO, accountNo);
                    session.setAttribute(Constant.ID, customer.getId());
                    session.setAttribute(Constant.ROLE,
                            customer.getRole().getRole());
                    session.setAttribute(Constant.NAME,
                            customer.getFirstName());
                    modelAndView.addObject(Constant.ACCOUNTS,
                            accountService.getAllAccounts());
                }
            } else {
                modelAndView = new ModelAndView(Constant.LOGIN);
                modelAndView.addObject(Constant.ERROR,
                        Constant.INVALID_CUSTOMER);
            }
        } catch (NullPointerException | AppException e) {
            modelAndView = new ModelAndView(Constant.LOGIN);
            modelAndView.addObject(Constant.ERROR, Constant.INVALID_CUSTOMER);
            SSTLog.Log(e);
        }

        return modelAndView;
    }

    /**
     * Get the email address and password from the user to reset the password
     * and return into the corresponding jsp page welcome page else redirect to
     * login page.
     * 
     * @param request
     *            the ServletRequest object that contains the customer request
     * @param response
     *            the ServletResponse object that contains the servlet's
     *            response
     * @return ModelAndView corresponding jsp page with status message.
     * @throws ServletException
     *             if an exception occurs that interferes with the servlet's
     *             normal operation
     * @throws IOException
     *             if an input or output exception occurs
     */
    @RequestMapping(params = "passwordreset")
    public ModelAndView resetPassword(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter(Constant.EMAIL);
        String newpassword = request.getParameter(Constant.NEWPASSWORD);
        ModelAndView modelAndView = null;
        try {
            modelAndView = new ModelAndView(Constant.FORGOT_PASSWORD,
                    Constant.MESSAGE,
                    loginService.resetPassword(email, newpassword));
        } catch (AppException e) {
            SSTLog.Log(e);
        }
        return modelAndView;
    }
}

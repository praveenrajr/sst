package com.ideas2it.sst.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.common.SSTLog;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Account;
import com.ideas2it.sst.model.Beneficiary;
import com.ideas2it.sst.model.Customer;
import com.ideas2it.sst.service.AccountService;
import com.ideas2it.sst.service.BeneficiaryService;
import com.ideas2it.sst.service.CustomerService;

/**
 * It represent the flow of beneficiary and receives the request and response to
 * the relative page.
 * 
 * @author Jothiprakash created on 2017-09-04.
 *
 */
@Controller
public class BeneficiaryController {

    @Autowired
    private BeneficiaryService beneficiaryService;

    @Autowired
    private CustomerService customerService;
    @Autowired
    private AccountService accountService;

    /**
     * Used to the delete the beneficiary which is added to the customer.
     * 
     * @param request
     *            the ServletRequest object that contains the beneficiary
     *            request.
     * @return ModelAndView corresponding jsp page with status message.
     * @throws ServletException
     *             if an exception occurs that interferes with the servlet's
     *             normal operation
     * @throws IOException
     *             if an input or output exception occurs
     */
    @RequestMapping("/deletebeneficiary")
    public ModelAndView deleteBeneficiary(HttpServletRequest request)
            throws ServletException, IOException {
        String Id = request.getParameter("id");
        ModelAndView modelAndView = new ModelAndView(Constant.WELCOME);
        try {
            beneficiaryService.deleteBeneficiaryById(Id);
            HttpSession session = request.getSession();
            Long id = (Long) session.getAttribute(Constant.ID);
            if (request.getParameter("action").equals("customer")) {
                Customer customer = customerService.getCustomerById(id);
                modelAndView.addObject(Constant.CUSTOMER_BENEFICIARIES,
                        customer.getBeneficiaries());
                modelAndView.addObject(Constant.CUSTOMER, customer);
            }
            if (request.getParameter("action").equals("admin")) {
                modelAndView.addObject(Constant.BENEFICIARIES,
                        beneficiaryService.getRequestedBeneficiaries());
                modelAndView.setViewName(Constant.WELCOMEADMIN);
            }
        } catch (AppException e) {
            modelAndView.addObject(Constant.ERROR, e);
            SSTLog.Log(e);
        }
        return modelAndView;
    }

    /**
     * It creates a new beneficiary object and redirect to the corresponding jsp
     * page.
     *
     * @param request
     *            the ServletRequest object that contains the beneficiary
     *            request.
     * @return ModelAndview object with view name and model object
     */
    @RequestMapping("/requestbeneficiary")
    public ModelAndView getBeneficiary(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String accountNo = (String) session.getAttribute(Constant.ACCOUNTNO);
        ModelAndView modelView = new ModelAndView(Constant.ADD_BENEFICIARY);
        try {
            modelView.addObject(Constant.ACCOUNT,
                    accountService.getAccountByNumber(accountNo));
            modelView.addObject(Constant.BENEFICIARY, new Beneficiary());
        } catch (AppException e) {
            SSTLog.Log(e);
        }
        return modelView;
    }

    /**
     * It creates the new record for beneficiary details from the view and store
     * into the record for approval.
     * 
     * @param beneficiary
     *            model attribute from view
     * @param request
     *            the ServletRequest object that contains the beneficiary
     *            request
     * @return ModelAndView object with view name and object.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     */
    @RequestMapping(value = "/addbeneficiary", method = RequestMethod.POST)
    public ModelAndView addBeneficiary(
            @ModelAttribute(Constant.BENEFICIARY) Beneficiary beneficiary,
            HttpServletRequest request) throws ServletException, IOException {
        ModelAndView modelAndView = new ModelAndView(Constant.WELCOME);
        Set<Customer> customers = new HashSet<>();
        try {
            HttpSession session = request.getSession();
            String accountNo = (String) session
                    .getAttribute(Constant.ACCOUNTNO);
            Account account = accountService.getAccountByNumber(accountNo);
            customers.add(account.getCustomer());
            beneficiary.setCustomers(customers);
            beneficiaryService.addBeneficiary(beneficiary);
            modelAndView.addObject(Constant.CUSTOMER_BENEFICIARIES,
                    account.getCustomer().getBeneficiaries());
            modelAndView.addObject(Constant.CUSTOMER, account.getCustomer());
            modelAndView.addObject(Constant.SUCCESS,
                    Constant.ADD_BENEFICIARY_SUCCESS);
        } catch (AppException e) {
            modelAndView.addObject(Constant.ERROR, e.getMessage());
            SSTLog.Log(e);
        }
        return modelAndView;
    }

    /**
     * get all beneficiaries that are waiting to get approval from admin.
     * 
     * @param request
     *            the ServletRequest object that contains the beneficiary
     *            request
     * @return ModelAndView object with view name and object.
     * @throws IOException
     *             if an input or output message is detected when the servlet
     *             handles the request.
     * @throws ServletException
     *             if the request could not be handled.
     */
    @RequestMapping("/getbeneficiaries")
    public ModelAndView getBeneficiaries(HttpServletRequest request)
            throws ServletException, IOException {
        ModelAndView modelAndView = new ModelAndView(Constant.WELCOMEADMIN);
        try {
            modelAndView.addObject(Constant.BENEFICIARIES,
                    beneficiaryService.getRequestedBeneficiaries());
        } catch (AppException e) {
            modelAndView.addObject(Constant.ERROR, e);
        }
        return modelAndView;
    }

    /**
     * Used to approve the beneficiary which will be added to the customer.
     * 
     * @param request
     *            the ServletRequest object that contains the beneficiary
     *            request.
     * @return ModelAndView corresponding jsp page with status message.
     * @throws ServletException
     *             if an exception occurs that interferes with the servlet's
     *             normal operation
     * @throws IOException
     *             if an input or output exception occurs
     */
    @RequestMapping("/approvebeneficiary")
    public ModelAndView approveBeneficiary(HttpServletRequest request)
            throws ServletException, IOException {
        String Id = request.getParameter("bId");
        ModelAndView modelAndView = new ModelAndView(Constant.WELCOMEADMIN);
        try {
            beneficiaryService.approveBeneficiaryById(Id);
            HttpSession session = request.getSession();
            modelAndView.addObject(Constant.BENEFICIARIES,
                    beneficiaryService.getRequestedBeneficiaries());
            modelAndView.addObject(Constant.SUCCESS, Constant.APPROVED_SUCCESS);
        } catch (AppException e) {
            modelAndView.addObject(Constant.ERROR, e);
            SSTLog.Log(e);
        }
        return modelAndView;
    }
}

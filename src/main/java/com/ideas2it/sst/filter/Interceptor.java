package com.ideas2it.sst.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * Interceptor is invoked before every request to process. Here Interceptor to
 * perform authentication for each request and response.
 * 
 * @author kalpana S created on 06-Sep-2017
 */
public class Interceptor extends HandlerInterceptorAdapter {

    /**
     * Called when each request and response is created.If no other filter is
     * available it redirect to resource page. Here it checks the session
     * availability if session exists it chains to next corresponding resource
     * else it redirects to login page.
     *
     * @param request
     *            Object which contains filter made by each request.
     * @param response
     *            Object which holds the filter response to corresponding
     *            entity.
     * @return True if, session exist, Else false.
     */
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {
        String uri = request.getRequestURI();
        switch (uri) {
        case "/SST/LoginController":
            response.setHeader("Cache-Control",
                    "no-cache, no-store, must-revalidate");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
            return true;
        case "/SST/createCustomer":
            return true;
        case "/SST/register":
            return true;
        default:
            try {
                HttpSession session = request.getSession(false);
                if (null != session.getAttribute("accountNo")) {
                    response.setHeader("Cache-Control",
                            "no-cache, no-store, must-revalidate");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
                    return true;
                } else {
                    response.sendRedirect("login");
                    return false;
                }
            } catch (NullPointerException e) {
                JOptionPane.showMessageDialog(null,
                        "Session Closed.Please Login", "Error",
                        JOptionPane.INFORMATION_MESSAGE);
                throw new Exception("Please Login");
            }
        }
    }
}
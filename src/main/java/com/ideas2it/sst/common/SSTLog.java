package com.ideas2it.sst.common;

import org.apache.log4j.Logger;

import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.exception.InputException;

public class SSTLog {
    private static Logger logger = Logger.getLogger(SSTLog.class);  
   
    public static void Log(Exception e) {
        logger.error("Exception", e);
    }
    
    public static void Log(InputException e) {
        logger.error("Exception", e);
    }
}

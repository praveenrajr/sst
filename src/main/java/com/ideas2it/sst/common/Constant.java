package com.ideas2it.sst.common;

/**
 * Contains constant values that are used commonly in anywhere.
 *
 * @author Jothiprakash created on 2017-09-11.
 */
public final class Constant {

    public static final int INITIAL_COUNT = 0;
    public static final boolean TRUE = true;
    public static final boolean FALSE = false;
    public static final String ID = "id";
    public static final String PROJECT_ASSIGNED_FAILURE = "Unable to assign the project.";
    public static final String EMAIL_ALREADY_EXIST = "Email already exist";
    public static final String DELETED_SUCCESS = "Deleted successfully";
    public static final String DELETED_FAILURE = "Unable to perform deletion operation";
    public static final String INVALID_EMAILID = "Please enter the valid email id";
    public static final String EXISTING_ID = "Please enter the existing id";
    public static final String UPDATED_SUCCESS = "Updated successfully";
    public static final String INSERT_SUCCESS = "Account Successfully added";
    public static final String ADDED_FAILURE = "Unable to add";

    public static final String ACCOUNTS = "accounts";
    public static final String ACCOUNT = "account";
    public static final String ACCOUNTNO = "accountNo";
    public static final String ROLES = "roles";
    public static final String LOGIN = "login";
    public static final String REGISTER = "register";
    public static final String ADDACCOUNT = "createAccount";
    public static final String ACTYPE = "types";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String NEWPASSWORD = "newpassword";
    public static final String FORGOT_PASSWORD = "forgot-password";
    public static final String ISCLOSED = "isClosed";
    public static final String CUSTOMER_INSERT_EXCEPTION = "Could not insert customer.Try again later ";
    public static final String CUSTOMER_NOT_EXIST = "Customer not exist in this account number";
    public static final String NOTVALID = "not valid";
    public static final String LOGIN_EXCEPTION = "Got an exception while login authenticate for the accountNo";
    public static final String PASSWORD_UPDATE_EXCEPTION = "Got an exception while updating password for the email";
    public static final String EMAIL_EXISTING_EXCEPTION = "Got an exception while checking existing for the email";
    public static final String ADDRESS_INSERT_FAILED = "Customer registration failed";
    
    // Invalid literals
    public static final String INVALID_NAME = "Name should be in alphabets";
    public static final String ADDRESS = "address";
    public static final String INVALID_DATE = "Please enter the valid date example (YYYY-MM-DD)";
    public static final String INVALID_COST = "Please enter the valid cost";
    public static final String INVALID_PHONENO = "Please enter the valid phone number";
    public static final String INVALID_ADHAARNO = "Adhaar number should be in tweleve digit numbers";
    public static final String INVALID_ACCOUNTNO = "Account number should be in thirteen digit number";
    public static final String ERROR = "error";
    public static final String CUSTOMER = "customer";
    public static final String WELCOMEADMIN = "welcomeAdmin";
    public static final String NAME = "name";
    
    // Success messages
    public static final String DELETE_SUCCESS = "Deleted successfully";
    public static final String REGISTER_SUCCESS = "Registered successfully";
    public static final String SUCCESS = "success";
    public static final String ACCOUNT_EXCEPTION = "Could not insert account.Try again later";
    public static final String APPROVED_SUCCESS = "Approved Successfully";
    public static final String AMOUNT_TRANSFER_SUCCESSFULLY = "Amount transfer successfully";

    // Invalid messages
    public static final String INVALID_CUSTOMER = "Account number and password mismatch";
    public static final String CUSTOMER_EXIST = "Customer already exist in this account";

    // Account entity messages
    public static final String INVALID_ACCOUNT = "Enter valid account number";
    public static final String ACCOUNT_NOT_EXIST = "Account number not exist";
    public static final String ACCOUNT_READ_EXCEPTION = "Could not read all accounts .Try again later";
    public static final String ACCOUNT_TYPE_EXCEPTION = "Could not read all account types .Try again later";
    public static final String ACCOUNT_NOT_FOUND = "Could not folnd account by number.Try again later";
    public static final String ACCOUNT_INSERT_EXCEPTION = "Could not insert this account number";
    public static final String ACCOUNT_UPDATE_EXCEPTION = "Could not close this account ";

    public static final String ACCOUNT_READ_USR_MESSAGE = "Could not read accounts.Try again later";
    public static final String INSERT_EXCEPTION = "Exception occured while inserting customer ";
    public static final String ROLE = "role";
    public static final String WELCOME = "welcome";
    public static final String NEW_PASSWORD = "newpassword";
    public static final String MESSAGE = "message";
    public static final String NOT_VALID = "notvalid";

    // Beneficiary entity message
    public static final String PROJECT_ASSIGNED_SUCCESS = "Project successfully assigned.";
    public static final String INVALID_EMAIL = "Please enter the valid email";

    public static final String ENTER_VALID_ID = "Enter valid account number";
    public static final String ID_NOT_EXIST = "Account number not exist";
    public static final String DEPOSIT_SUCCESS = "Deposited Successfully";
    public static final String DEPOSIT = "deposit";

    public static final String RETRIEVE_BENEFICIARY_EXCEPTION = "Got an exception while retrieving beneficiary for the id";
    public static final String UPDATE_BENEFICIARY_EXCEPTION = "Got an exception while updating beneficiary for the id";
    public static final String BENEFICIARY = "beneficiary";
    public static final String SAVE_BENEFICIARY_EXCEPTION = "Got an exception while saving beneficiary";
    public static final String CUSTOMER_BENEFICIARIES = "customerbeneficiaries";
    public static final String ADD_BENEFICIARY = "addbeneficiary";
    public static final String ISDELETED = "isDeleted";
    public static final String ISAPPROVED = "isApproved";
    public static final String CUSTOMERAPPROVALS = "customerApprovals";
    public static final String ADDADDRESS = "addAddress";
    public static final String CUSTOMER_UPDATE_EXCEPTION = "Could not update customer.Try again later";
    public static final String UPDATE_SUCCESS = "Updated successfully";
    public static final String INSERT_FAILED = "Insertion failed";
    public static final int ZERO = 0;
    public static final String CUSTOMER_ID = "customerId";
    public static final String ADDRESSES = "addresses";
    public static final String PERSONALDETAILS = "personalDetails";
    public static final String ADDRESS_INSERT_EXCEPTION = "Cannot insert address";
    public static final String CUSTOMER_EXCEPTION = "Could not read all customers";
    public static final String AMOUNT = "amount";
    public static final String IS_APPROVED = "isApproved";
    public static final String BENEFICIARY_READ_EXCEPTION = "Got an exception while retrieving requested beneficiaries";
    public static final String BENEFICIARIES = "beneficiaries";
    public static final String IFCCODE = "SST3737";
    public static final String TRANSACTION = "transaction";
    public static final String ADD_BENEFICIARY_SUCCESS = "Beneficiary request awaiting approval";
    
    // Address
    public static final String ADD_ADDRESS = "addAddress";
}

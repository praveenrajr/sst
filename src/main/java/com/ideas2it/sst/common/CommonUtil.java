package com.ideas2it.sst.common;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeParseException;

import com.ideas2it.sst.exception.AppException;

/**
 * <p>
 * To hold generic static methods for input validation. Those methods can called
 * from anywhere based on its requirement using name of the class.
 * </p>
 *
 * @author S.Kalpana created on 01/07/2017
 */
public class CommonUtil {

    /**
     * Checks the given string contains only alphabets.
     *
     * @param inputString
     *            Given input string.
     * @return True, if input contains alphabets, else false.
     */
    public static boolean isAlphabets(String inputString) {
        char[] characters = inputString.toCharArray();
        for (char character : characters) {
            if (!Character.isLetter(character) || character == ' ') {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks the given string in specified email format.
     *
     * @param email
     *            string Given input string.
     * @return True, if input matches with pattern. Else returns false.
     */
    public static boolean isValidEmail(String email) {
        return email.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+"
                + "(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    }

    /**
     * Checks the given string in valid phone number format.
     *
     * @param phoneNumber
     *            string input Given input contact number.
     * @return True if parameter match with pattern.
     */
    public static boolean isTenDigits(String phoneNumber) {
        if (isDigits(phoneNumber)
                || phoneNumber.matches("\\d{5}([- ]*)\\d{6}")) {
            return true;
        }
        return false;
    }

    /**
     * Checks given input contains only digits.
     *
     * @param inputString
     *            Given input contains any string format.
     * @return True if string contains only numbers else false.
     */
    public static boolean isDigits(String inputString) {
        char[] inputArray = inputString.toCharArray();
        for (char inputCharacter : inputArray) {
            if (Character.isDigit(inputCharacter)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks the given input string matches with given pattern date of birth
     * format.
     *
     * @param date
     *            Given input string.
     * @return True if parameter match with date pattern.
     * @throws AppException
     *             if, date of birth is invalid.
     */
    public static int dateDifference(String date) throws AppException {
        try {
            if (CommonUtil.isValidDate(date)) {
                LocalDate localDOB = LocalDate.parse(date);
                LocalDate currentDate = LocalDate.now();
                return Period.between(localDOB, currentDate).getYears();
            }
        } catch (DateTimeParseException exception) {
            throw new AppException("Invalid date format" + date, exception);
        }
        return 0;
    }

    /**
     * Checks the given date is in YYYY-MM-DD format with correct date, month
     * and year.
     *
     * @param date
     *            It indicates date which is going to validate.
     * @return True, if valid format. Else returns false.
     * @throws AppException
     *             if date not in format. Example February doesn't have 30 and
     *             31 date.
     */
    public static boolean isValidDate(String date) throws AppException {
        try {
            return date.matches("[0-9]{4}-(1[0-2]|0[1-9])-"
                    + "(3[01]|[1|2][0-9]|0[1-9])");
        } catch (DateTimeParseException exception) {
            throw new AppException("Invalid date format" + date, exception);
        }
    }
}

package com.ideas2it.sst.dao;

import java.util.List;

import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Account;
import com.ideas2it.sst.model.AccountType;

/**
 * <p>
 * It manages the overall process of account create, delete, update and read
 * operations.
 * </p>
 * 
 * @author Kalpana S created on 31/08/17.
 *
 */
public interface AccountDao {

    /**
     * It reads the all available account details.
     * 
     * @return List of all accounts
     * @throws AppException
     *             if, could not read all accounts.
     */
    List<Account> readAllAccounts() throws AppException;

    /**
     * Reads all available account types
     * 
     * @return List of available account type details.
     * @throws AppException
     *             if, could not read account types.
     */
    List<AccountType> readAllAccountTypes() throws AppException;

    /**
     * It reads the particular account details using their account number. If
     * account not exist in given id it returns null.
     * 
     * @param number
     *            It indicates the number of account to read its details.
     * @return Account object if exists or null.
     * @throws AppException
     *             if, could not read account details using id.
     */
    Account readAccountByNumber(Long number) throws AppException;

    /**
     * This method is used to create a new account record for persistent class
     * inputs.
     * 
     * @param account
     *            It indicates the account object with insert details.
     * @throws AppException
     *             if, could not insert account details.
     */
    void insertAccount(Account account) throws AppException;

    /**
     * It updates the account and corresponding customer to that account
     * 
     * @param account
     *            It indicates the object of persistent object.
     * @throws AppException
     *             if, could not read customer using account number.
     */
    void updateAccount(Account account) throws AppException;
}

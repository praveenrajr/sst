package com.ideas2it.sst.dao;

import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Address;

/**
 * It abstracts the create and update process of address details.
 * 
 * @author Kalpana S created on 04-Sep-2017
 */
public interface AddressDao {

    /**
     * Takes an address details for customer and create new record for address.
     *
     * @param address
     *            It denotes address details of customer.
     * @return Number of rows affected by this process.
     * @throws AppException
     *             if, cannot create address for given details.
     */
    int createAddress(Address address) throws AppException;
}

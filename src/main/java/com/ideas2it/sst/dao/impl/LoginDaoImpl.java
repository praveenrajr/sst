package com.ideas2it.sst.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.dao.LoginDao;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Account;
import com.ideas2it.sst.model.Customer;

/**
 * Authenticate the user and return the customer object if success object
 * contain customer detail else null.
 * 
 * @author Jothiprakash created on 29/08/17.
 *
 */
@Repository("loginDao")
public class LoginDaoImpl implements LoginDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * non-javadoc
     *
     * @see com.ideas2it.sst.dao.LoginDao #loginAuthenticate(Long, String)
     */
    @SuppressWarnings("deprecation")
    public Customer loginAuthenticate(Long accountNo, String password)
            throws AppException {
        Customer customer = null;
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Customer.class);
            criteria.add(Restrictions.eq(Constant.PASSWORD, password))
            .add(Restrictions.eq("isDeleted", 0))
            .add(Restrictions.eq("isApproved", 1));
            customer = (Customer) criteria.uniqueResult(); 
            if (null != customer) {
                criteria = session.createCriteria(Account.class);
                criteria.add(Restrictions.eq(Constant.ACCOUNTNO, accountNo))
                .add(Restrictions.idEq(customer.getAccount().getId()))
                .add(Restrictions.eq("isClosed",0));
                Account account = (Account) criteria.uniqueResult();
                return account.getCustomer();
            }
        } catch (HibernateException e) {
            throw new AppException(Constant.LOGIN_EXCEPTION + accountNo, e);
        }
        return customer;
    }

    /**
     * non-javadoc
     *
     * @see com.ideas2it.sst.dao.LoginDao#updatePassword(Customer)
     */
    public void updatePassword(Customer customer) throws AppException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(customer);
            transaction.commit();
        } catch (HibernateException e) {
            new AppException(
                    Constant.PASSWORD_UPDATE_EXCEPTION + customer.getEmail(),
                    e);
        }
    }

    /**
     * non-javadoc
     *
     * @see com.ideas2it.sst.dao.LoginDao#getCustomerByEmail(String)
     */
    @SuppressWarnings("deprecation")
    public Customer getCustomerByEmail(String email) throws AppException {
        Customer customer = null;
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Customer.class);
            criteria.add(Restrictions.eq(Constant.EMAIL, email));
            customer = (Customer) criteria.uniqueResult();
        } catch (HibernateException e) {
            new AppException(Constant.EMAIL_EXISTING_EXCEPTION + email, e);
        }
        return customer;
    }
}

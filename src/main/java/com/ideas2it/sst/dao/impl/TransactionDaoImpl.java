/**
 * 
 */
package com.ideas2it.sst.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.sst.dao.TransactionDao;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Role;
import com.ideas2it.sst.model.Transaction;

/**
 * <p>
 * It communicates with the repository to perform all operations with respect to
 * transaction class. And it implements the TransactionDao interface</p> 
 * 
 * @author Kalpana S created on 
 *
 */
@Repository("transactionDao")
public class TransactionDaoImpl implements TransactionDao {
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see com.ideas2it.sst.dao.TransactionDao#readAllTransactions()
     */
    @SuppressWarnings("deprecation")
    public List<Transaction> readAllTransactions() throws AppException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Transaction.class);
            return criteria.list();
        } catch (HibernateException e) {
            throw new AppException("Could not read transactions", e);
        }
    }
}

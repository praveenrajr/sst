package com.ideas2it.sst.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.dao.AddressDao;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Address;

/**
 * <p>
 * It communicates the data source to perform its operations. Also it implements
 * the AddressDao interface and overrides the all methods in that interface.
 *
 * @author Kalpana S created on 04-Sep-2017
 */
@Repository("addressDao")
public class AddressDaoImpl implements AddressDao {
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * non-javadoc
     *
     * @see com.ideas2it.sst.dao.AddressDao #createAddress(Address)
     * 
     */
    public int createAddress(Address address) throws AppException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(address);
        } catch (HibernateException e) {
            throw new AppException(Constant.ADDRESS_INSERT_EXCEPTION + address
                    .getCity(), e);
        }
        return 1;
    }
}

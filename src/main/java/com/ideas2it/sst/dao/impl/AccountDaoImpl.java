package com.ideas2it.sst.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.dao.AccountDao;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Account;
import com.ideas2it.sst.model.AccountType;

/**
 * <p>
 * It performs the all business logic operations for account details. In
 * addition to that it validates the given input details before communicates
 * with repository. It implements the AccounService class to invoke the
 * functionalities.
 * </p>
 * 
 * @author Kalpana S created on 31/08/17.
 *
 */
@Repository("accountDao")
public class AccountDaoImpl implements AccountDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see com.ideas2it.sst.dao.AccountDao#readAllAccounts()
     */
    @SuppressWarnings("deprecation")
    public List<Account> readAllAccounts() throws AppException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Account.class)
                    .add(Restrictions.eq(Constant.ISCLOSED, 0));
            return criteria.list();
        } catch (HibernateException e) {
            throw new AppException(Constant.ACCOUNT_READ_EXCEPTION, e);
        }
    }

    /**
     * @see com.ideas2it.sst.dao.AccountDao#readAllAccountTypes()
     *
     */
    @SuppressWarnings("deprecation")
    public List<AccountType> readAllAccountTypes() throws AppException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(AccountType.class);
            return criteria.list();
        } catch (HibernateException e) {
            throw new AppException(Constant.ACCOUNT_TYPE_EXCEPTION, e);
        }
    }

    /**
     * @see com.ideas2it.sst.dao.AccountDao#readAccountByNumber(Long)
     *
     */
    @SuppressWarnings("deprecation")
    public Account readAccountByNumber(Long number) throws AppException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Account.class)
                    .add(Restrictions.eq(Constant.ACCOUNTNO, number))
                    .add(Restrictions.eq(Constant.ISCLOSED, 0));
            return (Account) criteria.uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
            throw new AppException(Constant.ACCOUNT_NOT_FOUND + number, e);
        }
    }

    /**
     * @see com.ideas2it.sst.dao.AccountDao#insertAccount(Account)
     */
    public void insertAccount(Account account) throws AppException {
        try (Session session = sessionFactory.openSession()) {
            session.save(account);
        } catch (HibernateException e) {
            throw new AppException(
                    Constant.ACCOUNT_INSERT_EXCEPTION + account.getAccountNo(),
                    e);
        }
    }

    /**
     * @see com.ideas2it.sst.dao.AccountDao#updateAccount(Account)
     */
    @SuppressWarnings("deprecation")
    public void updateAccount(Account account) throws AppException {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.merge(account);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw new AppException(
                    Constant.ACCOUNT_UPDATE_EXCEPTION + account.getAccountNo(),
                    e);
        }
    }
}

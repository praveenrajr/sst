package com.ideas2it.sst.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.sst.dao.RoleDao;
import com.ideas2it.sst.model.Role;

/**
 * It used to abstract and encapsulate all access to data source.It communicates
 * with repository to perform operations.
 * 
 * @author Kalpana S created on 30-Aug-2017
 * 
 */
@Repository("roleDao")
public class RoleDaoImpl implements RoleDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @see com.ideas2it.sst.dao.RoleDao#readRoles()
     */
    @SuppressWarnings("deprecation")
    public List<Role> readRoles() {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Role.class);
        return criteria.list();
    }
}

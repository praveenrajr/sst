package com.ideas2it.sst.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.dao.CustomerDao;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.exception.InputException;
import com.ideas2it.sst.model.Account;
import com.ideas2it.sst.model.Address;
import com.ideas2it.sst.model.Customer;

/**
 * <p>
 * It used to abstract and encapsulate all access to data source. It manages
 * connection with data source to obtain stored data. And also it have all
 * access and modification permission of records in data store. It implements
 * the CustomerDao interface and overrides all methods in that interface.
 * </p>
 * 
 * @author Kalpana.S created on 29-08-2017
 *
 */
@Repository("customerDao")
public class CustomerDaoImpl implements CustomerDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * non-Javadoc
     * 
     * @see com.ideas2it.sst.dao.CustomerDao#insertCustomer(com.ideas2it.sst.model.Customer)
     *
     */
    public Long insertCustomer(Customer customer)
            throws AppException, InputException {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            Long id = (Long) session.save(customer);
            transaction.commit();
            return id;
        } catch (ConstraintViolationException e) {
            throw new InputException(Constant.CUSTOMER_EXIST
                    + customer.getAccount().getAccountNo(), e);
        } catch (HibernateException e) {
            e.printStackTrace();
            throw new AppException(Constant.CUSTOMER_INSERT_EXCEPTION
                    + customer.getFirstName(), e);
        }
    }

    /**
     * @see com.ideas2it.sst.dao.CustomerDao#readRegisteredCustomers()
     */
    @SuppressWarnings("deprecation")
    public List<Customer> readRegisteredCustomers() throws AppException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Customer.class)
                    .add(Restrictions.eq(Constant.ISDELETED, 0))
                    .add(Restrictions.eq(Constant.ISAPPROVED, 0));
            return criteria.list();
        } catch (HibernateException e) {
            throw new AppException(Constant.ACCOUNT_READ_EXCEPTION, e);
        }
    }

    /**
     * @see com.ideas2it.sst.dao.CustomerDao#getCustomerById(Long)
     */
    public Customer getCustomerById(Long id) throws AppException {
        try (Session session = sessionFactory.openSession()) {
            return (Customer) session.get(Customer.class, new Long(id));
        } catch (HibernateException e) {
            throw new AppException(Constant.CUSTOMER_NOT_EXIST + id, e);
        }
    }

    /**
     * @see com.ideas2it.sst.dao.CustomerDao#updateCustomer(Customer)
     */
    public void updateCustomer(Customer customer) throws AppException {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(customer);
            transaction.commit();
        } catch (HibernateException e) {
            throw new AppException(Constant.CUSTOMER_UPDATE_EXCEPTION, e);
        }
    }

    /**
     * @see com.ideas2it.sst.dao.CustomerDao#readAllCustomers()
     */
    public List<Customer> readAllCustomers() throws AppException {
        try (Session session = sessionFactory.openSession()) {

            Criteria criteria = session.createCriteria(Customer.class)
                    .add(Restrictions.eq(Constant.ISDELETED, 0))
                    .add(Restrictions.eq(Constant.ISAPPROVED, 1))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            return criteria.list();
        } catch (HibernateException e) {
            throw new AppException(Constant.CUSTOMER_EXCEPTION, e);
        }
    }
}

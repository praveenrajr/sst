/**
 * 
 */
package com.ideas2it.sst.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.sst.common.Constant;
import com.ideas2it.sst.dao.BeneficiaryDao;
import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Account;
import com.ideas2it.sst.model.Beneficiary;
import com.ideas2it.sst.model.Customer;

/**
 * It perform operations that will hit the database And also it have all access
 * and modification permission of records in data store. It implements the
 * BeneficiaryDao interface and overrides all methods in that interface.
 * 
 * @author Jothiprakash created on 2017-09-04.
 *
 */
@Repository("beneficiaryDao")
public class BeneficiaryDaoImpl implements BeneficiaryDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * non-Javadoc
     * 
     * @see com.ideas2it.sst.dao.BeneficiaryDao#updateBeneficiaryById(com.ideas2it.sst.model.Beneficiary)
     *
     */
    public void updateBeneficiaryById(Beneficiary beneficiary)
            throws AppException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(beneficiary);
            transaction.commit();
        } catch (HibernateException e) {
            throw new AppException(
                    Constant.UPDATE_BENEFICIARY_EXCEPTION + beneficiary.getId(),
                    e);
        }
    }

    /**
     * non-Javadoc
     * 
     * @see com.ideas2it.sst.dao.BeneficiaryDao#getBeneficiaryById(Long)
     *
     */
    @SuppressWarnings("deprecation")
    public Beneficiary getBeneficiaryById(Long id) throws AppException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Beneficiary.class)
                    .add(Restrictions.eq("id", id));
            return (Beneficiary) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new AppException(Constant.RETRIEVE_BENEFICIARY_EXCEPTION + id,
                    e);
        }
    }

    /**
     * non-Javadoc
     * 
     * @see com.ideas2it.sst.dao.BeneficiaryDao#saveBeneficiary(com.ideas2it.sst.model.Beneficiary)
     *
     */
    public void saveBeneficiary(Beneficiary beneficiary) throws AppException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(beneficiary);
            transaction.commit();
        } catch (HibernateException e) {
            throw new AppException(Constant.SAVE_BENEFICIARY_EXCEPTION
                    + beneficiary.getAccountNo(), e);
        }
    }

    /**
     * @see com.ideas2it.sst.dao.BeneficiaryDao#getRequestedBeneficiaries()
     */
    @SuppressWarnings("deprecation")
    public List<Beneficiary> getRequestedBeneficiaries() throws AppException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Beneficiary.class)
                    .add(Restrictions.eq(Constant.IS_APPROVED, 0))
                    .add(Restrictions.eq("isDeleted", 0));
            return criteria.list();
        } catch (HibernateException e) {
            throw new AppException(Constant.BENEFICIARY_READ_EXCEPTION, e);
        }
    }
}

/**
 * 
 */
package com.ideas2it.sst.dao;

import java.util.List;

import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Beneficiary;

/**
 * It manages overall CRUD operation for the beneficiary entity.
 * 
 * @author Jothiprakash created on 2017-09-04.
 *
 */
public interface BeneficiaryDao {

    /**
     * Get the beneficiary id and perform deletion operation for that id.
     * 
     * @param beneficiary
     *            Used to update the beneficiary relative to id.
     * @throws AppException
     *             if not able to delete beneficiary.
     */
    void updateBeneficiaryById(Beneficiary beneficiary) throws AppException;

    /**
     * Get the beneficiary by id to perform the update operation..
     * 
     * @param id
     *            Used to update the beneficiary relative to id.
     * @return Boolean status message for deletion.
     * @throws AppException
     *             if not able to delete beneficiary.
     */
    Beneficiary getBeneficiaryById(Long id) throws AppException;

    /**
     * Get the beneficiary details to store into the record.
     * 
     * @param beneficiary
     *            Object contains beneficiary details that will added into the
     *            record.
     * @throws AppException
     *             if not able to save beneficiary.
     */
    void saveBeneficiary(Beneficiary beneficiary) throws AppException;

    /**
     * Get the list of beneficiaries that are requested to get approval.
     * 
     * @return List<Beneficiary> list of beneficiaries to get approval.
     * @throws AppException
     *             if not able to retrieve beneficiary.
     */
    List<Beneficiary> getRequestedBeneficiaries() throws AppException;
}

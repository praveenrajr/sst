package com.ideas2it.sst.dao;

import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Customer;

/**
 * Authenticate the customer login and used to reset the customer password.
 * 
 * @author Jothiprakash created on 29/08/17.
 *
 */
public interface LoginDao {

    /**
     * Get the acccountNo and password to authenticate if accountNo and password
     * are matched it return the customer object contain the customer
     * information else object contain null.
     * 
     * @param accountNo
     *            userid to check if it is exist and match with password.
     * @param password
     *            password to check if it is exist and match with userid.
     * @return customer if accountNo and password are matched in the record
     *         return the customer object. else object contains null.
     * @throws AppException
     *             if failed to check the user id and password.
     */
    Customer loginAuthenticate(Long accountNo, String password)
            throws AppException;

    /**
     * Get the email address and new password to reset into the record
     * 
     * @param customer Object of the persistent class customer.
     * @throws AppException
     *             if failed to update new password.
     */
    void updatePassword(Customer customer) throws AppException;

    /**
     * Get the customer by email id.if email id not exist then the customer
     * object contain null else customer details
     * 
     * @param email
     *            To check whether it is exist in the record.
     * @return Customer contains customer details if email exist else object
     *         contains null.
     * @throws AppException
     *             if failed to retrieved the customer.
     */
    Customer getCustomerByEmail(String email) throws AppException;
}

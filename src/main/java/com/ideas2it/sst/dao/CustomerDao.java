package com.ideas2it.sst.dao;

import java.util.List;

import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.exception.InputException;
import com.ideas2it.sst.model.Address;
import com.ideas2it.sst.model.Customer;

/**
 * <p>
 * It exposes the overall process of customer create, update, delete and read
 * operations.
 * </p>
 * 
 * @author Kalpana S created on 29-08-2017
 *
 */
public interface CustomerDao {

    /**
     * It creates the new customer record using given customer details.
     * 
     * @param customer
     *            object transient details of persistent customer class.
     * @return Long id of inserted customer.
     * @throws AppException
     *             if, could not insert customer.
     * @throws InputException
     *             if, customer already exist in given account number.
     */
    Long insertCustomer(Customer customer) throws AppException, InputException;

    /**
     * Reads the all available customers who are not approved for online
     * transactions. If could not read customers.
     * 
     * @return list of all available customers.
     * @throws AppException
     *             if, could not read all customers.
     */
    List<Customer> readRegisteredCustomers() throws AppException;

    /**
     * Reads the customer details using their unique id.
     * 
     * @param id
     *            It denotes the unique identifier for each customer.
     * @return Object of persistent class customer.
     * @throws AppException
     *             if, customer read by id failed.
     */
    Customer getCustomerById(Long id) throws AppException;

    /**
     * It changes the existing customer details with new transient object of
     * customer.
     * 
     * @param customer
     *            with details to change.
     * @throws AppException
     *             if, customer update failed.
     */
    void updateCustomer(Customer customer) throws AppException;

    /**
     * Reads the all available customer details. If it fails it throws an
     * exception.
     * 
     * @return List of all available customers.
     * @throws AppException if, could not read all available customers.
     */
    List<Customer> readAllCustomers() throws AppException;
}

/**
 * 
 */
package com.ideas2it.sst.dao;

import java.util.List;

import com.ideas2it.sst.exception.AppException;
import com.ideas2it.sst.model.Transaction;

/**
 * It exposes the read process with respect to client.
 * 
 * @author ubuntu
 *
 */
public interface TransactionDao {

    /**
     * Reads the all available transactions from repository.
     * 
     * @return List of all available transactions.
     * @throws AppException
     *             if, could not read all transactions.
     */
    List<Transaction> readAllTransactions() throws AppException;

}

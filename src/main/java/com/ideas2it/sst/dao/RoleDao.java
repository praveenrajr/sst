package com.ideas2it.sst.dao;

import java.util.List;

import com.ideas2it.sst.model.Role;

/**
 * <p>
 * It encapsulates the overall process of role read opreation.
 * </p>
 * 
 * @author Kalpana S created on 30-Aug-2017
 * 
 */
public interface RoleDao {

    /**
     * It reads the all available roles from repository
     * 
     * @return List of all roles.
     */
    List<Role> readRoles();
}

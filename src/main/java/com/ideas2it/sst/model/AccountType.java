package com.ideas2it.sst.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * This is an account type entity which associates with accounts. It holds the
 * different type of accounts.
 * </p>
 * 
 * @author Kalpana.S created on 28-08-2017
 *
 */
@Entity
@Table(name = "account_type")
public class AccountType {
    private Long id;
    private String type;

    public AccountType() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return this.id;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "type")
    public String getType() {
        return this.type;
    }
}

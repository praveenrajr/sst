/**
 * 
 */
package com.ideas2it.sst.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * It represent the transaction and used to maintain the transactions for the
 * customer.
 * 
 * @author Jothiprakash created on 2017-09-01
 *
 */
@Entity
@Table(name = "transaction")
public class Transaction {
    private Long id;
    private String date;
    private int withdraw;
    private int deposit;
    private String description;
    private Long fromAccount;
    private Long closing_Balance;
    private Account account;
    private Beneficiary beneficiary;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, updatable = false, unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "date")
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Column(name = "withdraw", nullable = false, columnDefinition = "bigint default 0")
    public int getWithdraw() {
        return withdraw;
    }

    public void setWithdraw(int withdraw) {
        this.withdraw = withdraw;
    }
    
    @Column(name = "from_account")
    public Long getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(Long fromAccount) {
        this.fromAccount = fromAccount;
    }
    
    @Column(name = "closing_balance")
    public Long getClosing_Balance() {
        return closing_Balance;
    }

    public void setClosing_Balance(Long closing_Balance) {
        this.closing_Balance = closing_Balance;
    }

    @Column(name = "deposit", nullable = false, columnDefinition = "bigint default 0")
    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id")
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
    
    @ManyToOne
    @JoinColumn(name = "beneficiary_id")
    public Beneficiary getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }
}

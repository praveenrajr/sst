package com.ideas2it.sst.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterJoinTable;

/**
 * <p>
 * It represents the account details for customer and used to perform
 * transactions.
 * </p>
 * 
 * @author Kalpana.S created on 28-08-2017
 *
 */
@Entity
@Table(name = "account")
public class Account {
    private Long id;
    private Long accountNo;
    private AccountType accountType;
    private String dateOpened;
    private Long amount;
    private String IFCCode;
    private int isClosed;
    private Customer customer;
    private Set<Transaction> transactions;

    public Account() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, updatable = false, unique = true, nullable = false)
    public Long getId() {
        return this.id;
    }

    public void setAccountNo(Long accountNo) {
        this.accountNo = accountNo;
    }

    @Column(name = "account_no", unique = true)
	public Long getAccountNo() {
		return this.accountNo;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	@ManyToOne
	@JoinColumn(name = "type_id")
	public AccountType getAccountType() {
		return this.accountType;
	}

    @Column(name = "date_opened")
	public String getDateOpened() {
        return dateOpened;
    }

    public void setDateOpened(String dateOpened) {
        this.dateOpened = dateOpened;
    }

    public void setAmount(Long amount) {
		this.amount = amount;
	}

	@Column(name = "amount")
	public Long getAmount() {
		return this.amount;
	}

	public void setIFCCode(String IFCCode) {
		this.IFCCode = IFCCode;
	}

	@Column(name = "IFC_code")
	public String getIFCCode() {
		return this.IFCCode;
	}

	public void setIsClosed(int isClosed) {
		this.isClosed = isClosed;
	}

	@Column(name = "is_closed", nullable = false, columnDefinition = "tinyint default 0")
	public int getIsClosed() {
		return this.isClosed;
	}
    
	@OneToOne(mappedBy="account")
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
    
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, mappedBy = "account")
	public Set<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<Transaction> transactions) {
		this.transactions = transactions;
	}
}

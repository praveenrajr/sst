package com.ideas2it.sst.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

/**
 * Get the customer details to register for online banking.Used to represent the
 * transaction and account details for the customer to the corresponding object.
 * 
 * @author Jothiprakash created on 28/08/17
 *
 */
@Entity
@Table(name = "customer")
public class Customer {
    private Long id;
    private String firstName;
    private String lastName;
    private String password;
    private String gender;
    private String DOB;
    private String phoneNo;
    private String email;
    private Long adhaarNo;
    private Account account;
    private Role role;
    private int isApproved;
    private int isDeleted;
    private Set<Address> addresses;
    private Set<Beneficiary> beneficiaries;

    public Customer() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, unique = true)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "is_approved", nullable = false, insertable = false, columnDefinition = "tinyint default 0")
    public int getIsApproved() {
        return this.isApproved;
    }

    public void setIsApproved(int isApproved) {
        this.isApproved = isApproved;
    }

    @Column(name = "first_name")
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "last_name")
    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "password")
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "gender")
    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Column(name = "DOB")
    public String getDOB() {
        return this.DOB;
    }

    public void setDOB(String dOB) {
        this.DOB = dOB;
    }

    @Column(name = "phone_no")
    public String getPhoneNo() {
        return this.phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Column(name = "email")
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "adhaaar_no")
    public Long getAdhaarNo() {
        return this.adhaarNo;
    }

    public void setAdhaarNo(Long adhaarNo) {
        this.adhaarNo = adhaarNo;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="account_id", unique=true)  
	public Account getAccount() {
		return this.account;
	}

    public void setAccount(Account account) {
        this.account = account;
    }

    @ManyToOne
    @JoinColumn(name = "role_id")
    public Role getRole() {
        return this.role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "customer", cascade = CascadeType.ALL)
    public Set<Address> getAddresses() {
        return this.addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Column(name = "is_deleted", nullable = false, insertable = false, columnDefinition = "tinyint default 0")
    public int getIsDeleted() {
        return this.isDeleted;
    }
    @ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, mappedBy="customers")  
    public Set<Beneficiary> getBeneficiaries() {
        return beneficiaries;
    }

    public void setBeneficiaries(Set<Beneficiary> beneficiaries) {
        this.beneficiaries = beneficiaries;
    }
    
}

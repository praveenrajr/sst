Service Secure Transaction

Online banking project where customer can register for using online banking.Once registered customer can allowed to do transaction and view their transactions they can view their account details. Admin can monitor and perform overall functions

Prerequisites

1. Java 1.8
2. Mysql 5.7
3. Apache 8.5
4. Maven 2.0

Installing
 
  1) Unpack the archive where you would like to store the binaries, e.g.:

    Unix-based operating systems (Linux)
      tar zxvf apache-maven-2.x.y.tar.gz

  2) A directory called "apache-maven-2.x.y" will be created.

  3) Add the bin directory to your PATH, e.g.:

    Unix-based operating systems (Linux)
      export PATH=/usr/local/apache-maven-2.x.y/bin:$PATH

  4) Make sure JAVA_HOME is set to the location of your JDK

  5) Run "mvn --version" to verify that it is correctly installed.
 
 commands:
    1. mvn clean compile      //create all the class files for the project. 
    2. mvn clean package      //create the war file and compiled classes for the project. 

Authors

    Jothiprakash.A
    Kalpana.S
